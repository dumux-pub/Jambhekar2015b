// $Id: 2p-3c-Minproperties.hh 3784 2010-06-24 13:43:57Z bernd $
/*****************************************************************************
 *   Copyright (C) 2008 by Klaus Mosthaf, Andreas Lauser, Bernd Flemisch     *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPNCMinNIModel
 */
/*!
 * \file
 *
 * \brief Defines default values for most properties required by the
 *        2pncMinNI box model.
 */
#ifndef DUMUX_2PNCMINNI_PROPERTY_DEFAULTS_HH
#define DUMUX_2PNCMINNI_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/2pncmin/2pncminpropertydefaults.hh>

#include "2pncminnimodel.hh"
#include "2pncminniindices.hh"
#include "2pncminnifluxvariables.hh"
#include "2pncminnivolumevariables.hh"
#include "2pncminniproperties.hh"
#include "2pncminnilocalresidual.hh"

#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>

namespace Dumux
{

namespace Properties {
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////

/*!
 * \brief Set the property for the number of components.
 *
 * We just forward the number from the fluid system
 *
 */

//SET_INT_PROP(BoxTwoPNCMinNI, NumEq, 5); //!< set the number of equations to 5 
//Wrong for fuelcell model (NumEq=4)
//now set dynamically:
SET_PROP(BoxTwoPNCMinNI, NumEq)
{	
private:
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;
	
public:
    static const int value = FluidSystem::numComponents + FluidSystem::numSPhases + /*temperature*/1;
};

//! Use the 2p2cni local jacobian operator for the 2p2cni model
SET_TYPE_PROP(BoxTwoPNCMinNI,
              LocalResidual,
              TwoPNCMinNILocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(BoxTwoPNCMinNI, Model, TwoPNCMinNIModel<TypeTag>);

//! the VolumeVariables property
SET_TYPE_PROP(BoxTwoPNCMinNI, VolumeVariables, TwoPNCMinNIVolumeVariables<TypeTag>);


//! the FluxVariables property
SET_TYPE_PROP(BoxTwoPNCMinNI, FluxVariables, TwoPNCMinNIFluxVariables<TypeTag>);

//! The indices required by the non-isothermal 2p2c model
SET_PROP(BoxTwoPNCMinNI, Indices)
{ private:
    enum { formulation = GET_PROP_VALUE(TypeTag, Formulation) };
 public:
    typedef TwoPNCMinNIIndices<TypeTag, 0> type;
};

//! Somerton is used as default model to compute the effective thermal heat conductivity
SET_PROP(BoxTwoPNCMinNI, ThermalConductivityModel)
{ private :
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  public:
    typedef ThermalConductivitySomerton<Scalar> type;
};

//! DEPRECATED TwoPTwoCIndices and TwoPTwoCNIIndices properties
SET_TYPE_PROP(BoxTwoPNCMinNI, TwoPNCMinIndices, typename GET_PROP_TYPE(TypeTag, Indices));
SET_TYPE_PROP(BoxTwoPNCMinNI, TwoPNCMinNIIndices, typename GET_PROP_TYPE(TypeTag, Indices));


//! DEPRECATED SpatialParameters property
//SET_TYPE_PROP(BoxTwoPNCMinNI, SpatialParameters, typename GET_PROP_TYPE(TypeTag, SpatialParams));
}
}
#endif
