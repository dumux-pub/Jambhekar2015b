import matplotlib.pyplot as plt

expData=open('ExperimentalData-3.5M.txt', 'r')
splitFile=expData.read().split('\n')
time = [row.split('\t')[0] for row in splitFile] # time (Hrs)
time = [float(time[x])for x in range(len(time))]
wLoss = [row.split('\t')[1] for row in splitFile] # Total water loss (g)
wLoss = [float(wLoss[x])for x in range(len(wLoss))]
sPrec = [row.split('\t')[2] for row in splitFile] # Solid salt accumilation (g)
sPrec = [float(sPrec[x])for x in range(len(sPrec))]
###############################################
expData4_0=open('ExperimentalData-4.0M.txt', 'r')
splitFile4_0=expData4_0.read().split('\n')
time1 = [row.split(',')[0] for row in splitFile4_0] # time (Hrs)
time1 = [float(time1[x])for x in range(len(time1))]
wLoss1 = [row.split(',')[1] for row in splitFile4_0] # Total water loss (g)
wLoss1 = [float(wLoss1[x])* 1.2 for x in range(len(wLoss1))]
sPrec1 = [row.split(',')[2] for row in splitFile4_0] # Solid salt accumilation (g)
sPrec1 = [float(sPrec1[x]) for x in range(len(sPrec1))]
###############################################
expData6_0=open('ExperimentalData-6.0M.txt', 'r')
splitFile6_0=expData6_0.read().split('\n')
time2 = [row.split(',')[0] for row in splitFile6_0] # time (Hrs)
time2 = [float(time2[x])for x in range(len(time2))]
wLoss2 = [row.split(',')[1] for row in splitFile6_0] # Total water loss (g)
wLoss2 = [float(wLoss2[x]) for x in range(len(wLoss2))]
sPrec2 = [row.split(',')[2] for row in splitFile6_0] # Solid salt accumilation (g)
sPrec2 = [float(sPrec2[x]) for x in range(len(sPrec2))]
######################################################################################
readFile1=open('3.5M-indStudy.out', 'r')
splitFile1=readFile1.read().split('\n')
a1 = [row.split(';')[0] for row in splitFile1] # time
a1 = [float(a1[x])/3600 for x in range(len(a1))] # Convertion to (Hrs)
b1 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b1 = [float(b1[x])*3600 for x in range(len(b1))]  
c1 = [row.split(';')[2] for row in splitFile1] # Total water loss
c1 = [((float (c1[0])-float(c1[x]))/100)*0.8635*1e3 for x in range(len(c1))] #Scaling to domain and convertion to (g)
f1 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f1 = [(float(f1[x])/100)*0.8635*1e3 for x in range(len(f1))] # Scaling to domain and convertion to (g)
#########################################################################################
readFile1=open('4.0M-indStudy.out', 'r')
splitFile1=readFile1.read().split('\n')
a2 = [row.split(';')[0] for row in splitFile1] # time
a2 = [float(a2[x])/3600 for x in range(len(a2))] # Convertion to (Hrs)
b2 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b2 = [float(b2[x])*3600 for x in range(len(b2))]
c2 = [row.split(';')[2] for row in splitFile1] # Total water loss
c2 = [((float (c2[0])-float(c2[x]))/100)*0.8635*1e3 for x in range(len(c2))] #Scaling to domain and convertion to (g)
f2 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f2 = [(float(f2[x])/100)*0.8635*1e3 for x in range(len(f2))] # Scaling to domain and convertion to (g)
########################################################################################
readFile1=open('6.0M-indStudy.out', 'r')
splitFile1=readFile1.read().split('\n')
a3 = [row.split(';')[0] for row in splitFile1] # time
a3 = [float(a3[x])/3600 for x in range(len(a3))] # Convertion to (Hrs)
b3 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b3 = [float(b3[x])*3600 for x in range(len(b3))]
c3 = [row.split(';')[2] for row in splitFile1] # Total water loss
c3 = [((float (c3[0])-float(c3[x]))/100)*0.8635*1e3 for x in range(len(c3))] #Scaling to domain and convertion to (g)
f3 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f3 = [(float(f3[x])/100)*0.8635*1e3 for x in range(len(f3))] # Scaling to domain and convertion to (g)
######################################################################################
readFile1=open('3.5M-GuessValue-1.out', 'r')
splitFile1=readFile1.read().split('\n')
a4 = [row.split(';')[0] for row in splitFile1] # time
a4 = [float(a4[x])/3600 for x in range(len(a4))] # Convertion to (Hrs)
b4 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b4 = [float(b4[x])*3600 for x in range(len(b4))]
c4 = [row.split(';')[2] for row in splitFile1] # Total water loss
c4 = [((float (c4[0])-float(c4[x]))/100)*0.8635*1e3 for x in range(len(c4))] #Scaling to domain and convertion to (g)
f4 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f4 = [(float(f4[x])/100)*0.8635*1e3 for x in range(len(f4))] # Scaling to domain and convertion to (g)
#########################################################################################
readFile1=open('4.0M-GuessValue-1.out', 'r')
splitFile1=readFile1.read().split('\n')
a5 = [row.split(';')[0] for row in splitFile1] # time
a5 = [float(a5[x])/3600 for x in range(len(a5))] # Convertion to (Hrs)
b5 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b5 = [float(b5[x])*3600 for x in range(len(b5))]
c5 = [row.split(';')[2] for row in splitFile1] # Total water loss
c5 = [((float (c5[0])-float(c5[x]))/100)*0.8635*1e3 for x in range(len(c5))] #Scaling to domain and convertion to (g)
f5 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f5 = [(float(f5[x])/100)*0.8635*1e3 for x in range(len(f5))] # Scaling to domain and convertion to (g)
########################################################################################
readFile1=open('6.0M-GuessValue-1.out', 'r')
splitFile1=readFile1.read().split('\n')
a6 = [row.split(';')[0] for row in splitFile1] # time
a6 = [float(a6[x])/3600 for x in range(len(a6))] # Convertion to (Hrs)
b6 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b6 = [float(b6[x])*3600 for x in range(len(b6))]
c6 = [row.split(';')[2] for row in splitFile1] # Total water loss
c6 = [((float (c6[0])-float(c6[x]))/100)*0.8635*1e3 for x in range(len(c6))] #Scaling to domain and convertion to (g)
f6 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f6 = [(float(f6[x])/100)*0.8635*1e3 for x in range(len(f6))] # Scaling to domain and convertion to (g)
########################################################################################
readFile1=open('3.5M-GuessValue-2.out', 'r')
splitFile1=readFile1.read().split('\n')
a7 = [row.split(';')[0] for row in splitFile1] # time
a7 = [float(a7[x])/3600 for x in range(len(a7))] # Convertion to (Hrs)
b7 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b7 = [float(b7[x])*3600 for x in range(len(b7))]
c7 = [row.split(';')[2] for row in splitFile1] # Total water loss
c7 = [((float (c7[0])-float(c7[x]))/100)*0.8635*1e3 for x in range(len(c7))] #Scaling to domain and convertion to (g)
f7 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f7 = [(float(f7[x])/100)*0.8635*1e3 for x in range(len(f7))] # Scaling to domain and convertion to (g)
#########################################################################################
readFile1=open('4.0M-GuessValue-2.out', 'r')
splitFile1=readFile1.read().split('\n')
a8 = [row.split(';')[0] for row in splitFile1] # time
a8 = [float(a8[x])/3600 for x in range(len(a8))] # Convertion to (Hrs)
b8 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b8 = [float(b8[x])*3600 for x in range(len(b8))]
c8 = [row.split(';')[2] for row in splitFile1] # Total water loss
c8 = [((float (c8[0])-float(c8[x]))/100)*0.8635*1e3 for x in range(len(c8))] #Scaling to domain and convertion to (g)
f8 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f8 = [(float(f8[x])/100)*0.8635*1e3 for x in range(len(f8))] # Scaling to domain and convertion to (g)
########################################################################################
readFile1=open('6.0M-GuessValue-2.out', 'r')
splitFile1=readFile1.read().split('\n')
a9 = [row.split(';')[0] for row in splitFile1] # time
a9 = [float(a9[x])/3600 for x in range(len(a9))] # Convertion to (Hrs)
b9 = [row.split(';')[1] for row in splitFile1]# Evaporation rate
b9 = [float(b9[x])*3600 for x in range(len(b9))]
c9 = [row.split(';')[2] for row in splitFile1] # Total water loss
c9 = [((float (c9[0])-float(c9[x]))/100)*0.8635*1e3 for x in range(len(c9))] #Scaling to domain and convertion to (g)
f9 = [row.split(';')[5] for row in splitFile1] # Solid salt accumilation
f9 = [(float(f9[x])/100)*0.8635*1e3 for x in range(len(f9))] # Scaling to domain and convertion to (g)
########################################################################################
#LEGEND LOCATION
#upper right	loc=1
#upper left	loc=2
#lower left	loc=3
#lower right	loc=4
########################################################################################
###=========================CUM WATER EVAPORATION-3.5==================================
#fig2 = plt.figure()
#ax2 = fig2.add_subplot(111)
##ax2.set_title("Cumulative Water Loss", fontsize=20)
#ax2.set_xlabel('Time [h]', fontsize=45)
#ax2.set_ylabel('Water [g]', fontsize=45)
#ax2.set_xlim([0, 20])
#ax2.set_ylim([0, 0.8])
#ax2.tick_params(labelsize=45)
#ax2.plot(a1,c1,'bs', label='20H_3.5M_305.15K',markersize=20)
#ax2.plot(time,wLoss,'ko', label='expData-3.5M',markersize=20)
#leg = ax2.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
#ax2.grid(True)
#plt.show()
###==========================PRECIPITATED SALT STORAGE-3.5===========================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation (3.5 [M])", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Salt [g]', fontsize=45)
ax3.set_ylim([0, 0.2])
ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a1,f1,'rs', label='kp= 9e-3_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a4,f4,'b^', label='kp= 1e-1_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a7,f7,'g*', label='kp= 1e-2_np=1.3_nsw_1.0',markersize=20)
ax3.plot(time,sPrec,'ko', label='expData-3.5M',markersize=20)
leg = ax3.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=30)
ax3.grid(True)
plt.show()
#=========================CUM WATER EVAPORATION-4.0==============================
#fig2 = plt.figure()
#ax2 = fig2.add_subplot(111)
##ax2.set_title("Cumulative Water Loss", fontsize=20)    
#ax2.set_xlabel('Time [h]', fontsize=45)
#ax2.set_ylabel('Water [g]', fontsize=45)
#ax2.set_xlim([0, 20])
#ax2.set_ylim([0, 0.8])
#ax2.tick_params(labelsize=45)
#ax2.plot(a2,c2,'gs', label='20H_4.0M_305.15K',markersize=20)
#ax2.plot(time1,wLoss1,'ko', label='expData-4.0M',markersize=20)
#leg = ax2.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
#ax2.grid(True)
#plt.show()
#==========================PRECIPITATED SALT STORAGE-4.0===========================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Salt [g]', fontsize=45)
ax3.set_ylim([0, 0.2])
ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a2,f2,'rs', label='kp= 9e-3_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a5,f5,'b^', label='kp= 1e-1_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a8,f8,'g*', label='kp= 1e-2_np=1.3_nsw_1.0',markersize=20)
ax3.plot(time1,sPrec1,'ko', label='expData-4.0M',markersize=20)
leg = ax3.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=30)
ax3.grid(True)
plt.show()
#=========================CUM WATER EVAPORATION-6.0==============================
#fig2 = plt.figure()
#ax2 = fig2.add_subplot(111)
##ax2.set_title("Cumulative Water Loss", fontsize=20)    
#ax2.set_xlabel('Time [h]', fontsize=45)
#ax2.set_ylabel('Water [g]', fontsize=45)
#ax2.set_xlim([0, 20])
#ax2.set_ylim([0, 0.8])
#ax2.tick_params(labelsize=45)
#ax2.plot(a3,c3,'rs', label='20H_6.0M_305.15K',markersize=20)
#ax2.plot(time2,wLoss2,'ko', label='expData-6.0M',markersize=20)
#leg = ax2.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=45)
#ax2.grid(True)
#plt.show()
###==========================PRECIPITATED SALT STORAGE-6.0===========================
fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Salt [g]', fontsize=45)
ax3.set_ylim([0, 0.2])
ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a3,f3,'rs', label='kp= 9e-3_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a6,f6,'b^', label='kp= 1e-1_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a9,f9,'g*', label='kp= 1e-2_np=1.3_nsw_1.0',markersize=20)
ax3.plot(time2,sPrec2,'ko', label='expData-6.0M',markersize=20)
leg = ax3.legend(loc=2,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=30)
ax3.grid(True)
plt.show()
###==========================EvapRate=========================================
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

fig3 = plt.figure()
ax3 = fig3.add_subplot(111)
#ax3.set_title("Cumulative Salt Precipitation", fontsize=20)    
ax3.set_xlabel('Time [h]', fontsize=45)
ax3.set_ylabel('Evap.Rate [mm/h]', fontsize=45)
ax3.set_ylim([0, 0.5])
ax3.set_xlim([0, 20])
ax3.tick_params(labelsize=45)
ax3.plot(a1,b1,'r-', label='3.5M-kp= 9e-3_np=1.3_nsw_1.0',lw=8)
ax3.plot(a4,b4,'b-', label='3.5M-kp= 1e-1_np=1.3_nsw_1.0',lw=8)
ax3.plot(a7,b7,'g-', label='3.5M-kp= 1e-2_np=1.3_nsw_1.0',lw=8)
ax3.plot(a2,b2,'r*', label='4.0M-kp= 9e-3_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a5,b5,'b*', label='4.0M-kp= 1e-1_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a8,b8,'g*', label='4.0M-kp= 1e-2_np=1.3_nsw_1.0',markersize=20)
ax3.plot(a3,b3,'ro', label='6.0M-kp= 9e-3_np=1.3_nsw_1.0',markersize=10)
ax3.plot(a6,b6,'bo', label='6.0M-kp= 1e-1_np=1.3_nsw_1.0',markersize=10)
ax3.plot(a9,b9,'go', label='6.0M-kp= 1e-2_np=1.3_nsw_1.0',markersize=10)
leg = ax3.legend(loc=3,handlelength=2, borderpad=1.5, labelspacing=1.5, fontsize=20)
ax3.grid(True)
plt.show()