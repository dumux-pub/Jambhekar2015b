#ifndef KCLCHEM_HH
#define KCLCHEM_HH

// Brine Salt Precipitation Chemistry...

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>	       // for the specific Na,Cl component info.
#include <dumux/material/fluidsystems/naclkfluidsystem.hh>  //properties of the NaCl fluid system as whole.
#include <dumux/implicit/2pncmin/2pncminproperties.hh>
#include <cmath>
#include <iostream>

namespace Dumux
{
/*!
* \The equilibrium chemistry is calculated in this class. The function to calculateEquilbriumChemistry is used to control the Newton Solver "newton1D". The chemical functions and derivations are implemented in the private part of
class. */

/*SET_PROP(Brine, Fluid)
        {
        private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
        public:
    typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
        };*/

template <class TypeTag>
class KClChem
{																	//private defined
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

typedef KClChem <TypeTag> ThisType;

public:												//including the following into the class.
/*try
{
kprec_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, NaClcoefficients, kprec);
nprec_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, NaClcoefficients, nprec);
nSw_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, NaClcoefficients, nSw);
}
catch (Dumux::ParameterException &e) {
    std::cerr << e << ". Abort!\n";
    exit(1) ;
}*/ //try,  catch added for the iTOUGH2 part. Not needed for now.
static const int wPhaseIdx = FluidSystem::wPhaseIdx;
static const int nPhaseIdx = FluidSystem::nPhaseIdx;
static const int wCompIdx = FluidSystem::wCompIdx;
static const int nCompIdx = FluidSystem::nCompIdx;
static const int H2OIdx = FluidSystem::H2OIdx;
//static const int NaIdx = FluidSystem::NaIdx;
static const int ClIdx = FluidSystem::ClIdx;
static const int KIdx = FluidSystem::KIdx;
static const int sPhaseIdx = FluidSystem::KClPhaseIdx;  //for the precipitation
static const int precipKClIdx = FluidSystem::numComponents;  //for the precipitation


static const int numComponents = FluidSystem::numComponents;
static const int numMajorComponents = FluidSystem::numMajorComponents;
static const int numSecComponents = FluidSystem::numSecComponents;
static const int numTotComponents = numComponents + numSecComponents;
static const int numPhases = FluidSystem::numPhases;


typedef Dune::FieldVector<Scalar, 3> Vector;
typedef Dune::FieldVector<Scalar, 2> SolVector;
typedef Dune::FieldVector<Scalar, numTotComponents> CompVector;

//    Return equlibrium constant for dissolution reaction:
//    Na(s)+ Cl(g) <--> NaCl(s)
//    K(s) + Cl(g) <---> KCl(s)

static Scalar solubilityProduct()
{
    return 17.63189; // mol²/kg² @ 25°C
    /* The solubility product here is defined with the conversion from [mol²/l²] to
    [mol²/kg²] with the conversion as follows:
    Ksp = 23.... mol²/l² and by density(1.15kg/m³) = mass / volume (10^-3 m³). So, dividing the initial
    Ksp with {38.6890/(1.15*1.15)}  */ //return 17.63189; //changed according to the temperature..			//return(37.8594); 			// 6.15*6.15 moles per litre.(360g/lt).
}

//static Scalar massFracToMolality(const Scalar massFracX, const Scalar molarMassX, const Scalar massFracC, const Scalar massFracSalinity)
//{
//Scalar molalityX = (massFracX)/(molarMassX*(1- massFracX - massFracSalinity));
//return molalityX; 					// from Wiki....
//}

/*!* Returns the mass fraction of a component x (kg x / kg solution) for a given molality fraction (mol x / mol solution).* The salinity and the mole Fraction of CO2 are considered* */

//static Scalar molalityToMassFrac(Scalar molalityX, Scalar molarMassX, Scalar massFracSalinity)
//{
//Scalar massFracX = (molalityX * molarMassX * (1- massFracSalinity)) / (1+(molalityX*molarMassX));
//return massFracX; 			// modification of the above formula...
//}


static Scalar moleFracToMolality(Scalar moleFracX, Scalar moleFracSalinity)
{
Scalar molalityX = (moleFracX) / ((1-/*moleFracX*/ moleFracSalinity)* FluidSystem::molarMass(H2OIdx));
return molalityX;
}   	/*commented moleFracX because it is subtracting the secondary Na, Cl component twice*/



//static Scalar molalityToMoleFrac(Scalar molalityX, Scalar moleFracSalinity)
//{
//Scalar moleFracX = (molalityX * FluidSystem::molarMass(H2OIdx) * (1 - moleFracSalinity))/(1+(molalityX * FluidSystem::molarMass(H2OIdx)));
//return moleFracX; 		//modification of the above formula...
//}


static Scalar ionicStrength(const VolumeVariables &volVars)
{
    Scalar ionicStrengthkcl_=0;
    for (int compIdx= numMajorComponents; compIdx < numComponents; ++compIdx)
        /*start here with the major components as the for h2o and air the charge is 0.*/
    {
        Scalar molality = moleFracToMolality(volVars.moleFraction(wPhaseIdx, compIdx), volVars.moleFracSalinity());
        ionicStrengthkcl_ +=  molality * FluidSystem::charge(compIdx)* FluidSystem::charge(compIdx);
        ionicStrengthkcl_ *= 0.5;
    }
    return (ionicStrengthkcl_);
}

/*static Scalar ionicStrengthNa()
{
Scalar ionicStrength = 0.0;
//Scalar molality = 500;
//Scalar charge = 1;
ionicStrength += molality_ * charge_ * charge_;
ionicStrength *= 0.5;
return ionicStrength;
}
// Commented as the Ionic Strength (I) is'nt a component property but it is a phase property.
 *
static Scalar ionicStrengthCl()
{

Scalar ionicStrength = 0.0;
//Scalar molality=500;
//Scalar charge =1;
ionicStrength += molality_ * charge_ * charge_;
ionicStrength *= 0.5;
return ionicStrength;
}*/


//Calculates the activity with a modified Debye-Hückel equation after Parkhurst (1990) for ionic strengths up to 2.//

static Scalar activityCoefficient(Scalar ionicStrengthkcl_, Scalar temperatureK, int compIdx)
{
if (ionicStrengthkcl_<0)
{ 							//For the Newton iteration.
ionicStrengthkcl_ = 0;
}
//
///* Scalar temperatureC = temperatureK - 273.15; //Temperature in °C
//Finding density relation:Scalar d=1-pow((temperatureC – 3.9863),2)*(temperatureC + 288.9414)/(508929.2*(temperatureC + 68.12963))+0.011445*exp(-374.3 / temperatureC);
//// dielectric constant:
//Scalar eps=2727.586+0.6224107*temperatureKconst-466.9151*log(temperatureK)-52000.87/temperatureK;
////A, B are temperature dependent parameters for the Debye-Hückel equation
//// Scalar A = 1.82483e6 * sqrt(d)/pow(eps*temperatureK, 1.5);
//// Scalar B = 50.2916*sqrt(d)/pow(eps*temperatureK, 0.5);*/
//temperature dependent parameters A and B.
Scalar A = 0.5085;
Scalar B = 0.3281e8;


Scalar charge = FluidSystem::charge(compIdx);
Scalar ai = FluidSystem::ai(compIdx);
Scalar bi = FluidSystem::bi(compIdx);
//
//
//// The actual modified Debye Hückel equation that is being applied.
//
//
Scalar logActivityCoefficient = -A*pow(charge, 2)*sqrt(ionicStrengthnacl_)/(1 + (B*ai*sqrt(ionicStrengthnacl_)))
                        + (bi*ionicStrengthnacl_);
                        /*commented to check if the molality
                        is only having the effect and activity has no impact over the change
                        in values*/

return pow(10, logActivityCoefficient);
}

// if (compIdx==NaIdx) {
//Scalar logActivityCoefficientNa = -A*pow(charge, 2.)*sqrt(ionicStrengthnacl_)/(1 + B*ai*sqrt(ionicStrengthnacl_))
//                        + bi*ionicStrengthnacl_;
//
//return pow(10, logActivityCoefficientNa);
//}
//
// for (int compIdx= numMajorComponents; compIdx <= numComponents; ++compIdx)
// {
//	 Scalar logActivityCoefficient = 0;
//	 logActivityCoefficient += -A*pow(charge, 2.)*sqrt(ionicStrength)/(1 + B*ai*sqrt(ionicStrength))
//                                     + bi*ionicStrength;
//	 return pow(10, logActivityCoefficient);
// }
//
//
//if (compIdx==ClIdx) {
//Scalar logActivityCoefficientCl = -A*pow(charge, 2.)*sqrt(ionicStrength)/(1 + B*ai*sqrt(ionicStrength))
//                        + bi*ionicStrength;
//
//return pow(10, logActivityCoefficientCl);
//					}	// return 1 incase of error.
//}


void reactionSource(PrimaryVariables &q,
        const VolumeVariables &volVars)
{

q = 0;

//define and compute some parameters for simplicity:

Scalar xlSalinity = volVars.moleFracSalinity();
/* Scalar molarDensity = volVars.molarDensity(wPhaseIdx);
 * not being used anywhere*/
Scalar initialPorosity = volVars.InitialPorosity();
/* Scalar porosity = volVars.porosity();
 * we are specifying the initial porosity and thereby it is calculating. So, no need to
 * again mention this.*/
Scalar Sw = volVars.saturation(wPhaseIdx);
Scalar VolFracPrecipitation = std::min(initialPorosity - 0.101, volVars.solidity(sPhaseIdx));

if (VolFracPrecipitation < 1e-20)
VolFracPrecipitation = 0;

Scalar mNa = moleFracToMolality(volVars.moleFraction(wPhaseIdx,NaIdx), xlSalinity)
            * activityCoefficient(ionicStrength(volVars),volVars.temperature(), NaIdx); //[mol_sodium/kg_H2O]

if (mNa < 1e-20)
mNa = 0;

Scalar mCl = moleFracToMolality(volVars.moleFraction(wPhaseIdx,ClIdx), xlSalinity)
           * activityCoefficient(ionicStrength(volVars),volVars.temperature(), ClIdx);  //[mol_Cl/kg_H2O]

if (mCl < 1e-20)
mCl = 0;
//mCl = mNa;
//std::cout<<"mNa: "<< mNa <<"mCl: "<< mCl << "mNa*mCl: "<< mNa*mCl << std::endl;		// added by vishal for checking

// compute dissolution and precipitation rate of Na,Cl.

Scalar Ksp =  solubilityProduct();
Scalar Omega = (mNa * mCl) / Ksp;

/*std::cout << "Sw = " << Sw << std::endl;
std::cout<<"mNa ="<< mNa<<std::endl;
std::cout<<"mCl ="<< mCl<< std::endl;
std::cout << "Saturation Index = "<< Omega <<std::endl;
std::cout<< "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ "<<std::endl;*/
// Asw: defines the area at the interface b/w Solid and Water...
// Acw: defines the area at the interface b/w salt precipitated and Water...
Scalar Asw = Asw0_ * pow(1-((VolFracPrecipitation)/initialPorosity),(2.0/3.0));
Asw *= pow(Sw,nSw_);
/* Scalar Acw = 0;
if (ac_ * VolFracPrecipitation < Asw) // find the ac_ which is the surface ar. Of the salt crystal.
{
Acw = ac_ * VolFracPrecipitation;
}
else Acw = Asw;
 * for now not yet implemented for the dissolution part.*/
Scalar rdiss = 0;

Scalar rprec = 0;

if (Omega >= 1)
{
rdiss = 0;
rprec = kprec_ * Asw * pow(Omega - 1 , nprec_);//[mol/dm³s]
rprec *= 1000; // rprec [mol/m³s]
}

/*else
{
rdiss = (kdiss1_ * 1 + kdiss2_) * Acw * pow((1 - Omega), ndiss_); //[mol/dm³s]
rdiss *= 1000; // rdiss [mol/m³s]
rprec = 0;
}*/

/*std::cout<<"rprec: "<< rprec << std::endl
         <<"rdiss: "<< rdiss << std::endl
         <<"Omega: "<< Omega << std::endl;*/

//std::cout<<"Omega: "<< Omega << std::endl;

// r [mol/m³s]
// q [mol/mol] for transported components
// q [m³/m³] for solid phases
q[wCompIdx] = 0.0; //IN ÄNDERUNG DES MOLENBRUCHS UMRECHNEN-Water as const.
q[nCompIdx] = 0.0;//Air % inc. When dissolution and dec when precip.
q[NaIdx] = (-rprec + rdiss);
q[ClIdx] = (-rprec + rdiss);
q[precipKClIdx] = (rprec - rdiss);

/*if (std::isnan(q[NaIdx]))
  std::cout << "rprec = " << rprec << ", Asw0_ = " << Asw0_
            << ", VolFracPrecipitation = " << VolFracPrecipitation << ", pow(1-((VolFracPrecipitation)/initialPorosity),(2.0/3.0)) = " << pow(1-((VolFracPrecipitation)/initialPorosity),(2.0/3.0))
            << ", Sw " << Sw << ", nSw_ = " << nSw_ << ", initialPorosity = " << initialPorosity << std::endl; */
// above added to check for error in the chemistry use when needed.
}
private:

/* Newton 1D Method in this case is being used for the calculation of the equilibrium chemistry incase of complex compounds which have a tendency to split into various small components like H2O-> H+ + OH-. */


/*bool newton1D(Scalar &xVar, void (ThisType::*funcPtr)(Scalar), const Scalar tolAbs, const Scalar tolRel, const int maxIter)
{
(this->*funcPtr)(xVar);

Scalar h = -fdf_[0]/fdf_[1]; // h = x(i) - x(i-1)
Scalar hLast = h*0.5; //initial Step
iter_ = 200;
bool converge = false;
if (std::isnan(h))
{
return converge = false;
}

while(absolute(h) > tolAbs || absolute(h/hLast) > 1 + tolRel) //TODO
{
if(iter_ > maxIter){break;}

if(iter_ > 0)
{
(this->*funcPtr)(xVar);
hLast = h;
h = -fdf_[0]/fdf_[1];
}
if (std::isnan(h))
{
return converge = false;
}

xVar = xVar + h;
iter_ = iter_ + 1;
}
if(xVar < 0.0) {return converge = false;}
if(iter_ <= maxIter) {converge = true;}
return converge;

}*/

/*Bisection Method Solver returns true if convergence is reached and false if not.
xVar is the variable for which the system is solved funcPtr is the pointer to the function which is to be solved a0 is the lower starting value, b0 is the upper starting value. The root must be inside the interval [a0, b0] tol is the stopping critium a-b */

//bool bisection1D(Scalar &xVar, void (ThisType::*funcPtr)(Scalar), const Scalar a0, const Scalar b0, const Scalar tol)
//{
//Scalar iterNo = 0;
//int maxIter = 200;
//bool converge = false;
//int sfb, sfx;
//
//Scalar a = a0;
//Scalar b = b0;
//(this->*funcPtr)(b);
//sfb = sign(fdf_[0]);
//
//while(b-a > tol)
//{
//if(iterNo > maxIter)
//{
//return converge;
//}
//xVar = (b + a)/2;
//(this->*funcPtr)(xVar);
//sfx = sign(fdf_[0]);
//iterNo = iterNo + 1;
//if (sfx == 0)
//break;
//	else
//	{//bool bisection1D(Scalar &xVar, void (ThisType::*funcPtr)(Scalar), const Scalar a0, const Scalar b0, const Scalar tol)
//{
//Scalar iterNo = 0;
//int maxIter = 200;
//bool converge = false;
//int sfb, sfx;
//
//Scalar a = a0;
//Scalar b = b0;
//(this->*funcPtr)(b);
//sfb = sign(fdf_[0]);
//
//while(b-a > tol)
//{
//if(iterNo > maxIter)
//{
//return converge;
//}
//xVar = (b + a)/2;
//(this->*funcPtr)(xVar);
//sfx = sign(fdf_[0]);
//iterNo = iterNo + 1;
//if (sfx == 0)
//break;
//	else
//	{
//		if(sfx == sfb)
//		{
//		b = xVar;
//		}
//	else
//	{
//	a = xVar;
//	}
//	}
//}
//converge = true;
//return converge;
//}
//		if(sfx == sfb)
//		{
//		b = xVar;
//		}
//	else
//	{
//	a = xVar;
//	}
//	}
//}
//converge = true;
//return converge;
//}

// void quadPoly(Scalar a, Scalar b, Scalar c)
// {
// x_ = (-b + sqrt(pow(b, 2) - 4*a*c))/2/a;
// y_ = (-b - sqrt(pow(b, 2) - 4*a*c))/2/a;
// }

/*
Scalar absolute(Scalar x)		// for the newton 1D iterator usage...
{
if(x<0.0)
{
return x*(-1);
}
*/

Scalar sign(Scalar x) 		// for the bisection method usage...
{
    if(x > 0.0)
    {
    return 1;
    }
        else if (x < 0.0)
        {
        return -1;
        }
        else
        {
        return 0.0;
        }
}

//int iter_; //Number of iterations the Newton solver needs until convergence


//Scalar ionicStrengthnacl_;

SolVector fdf_; /*Solution vector for the solvers every equation f solved by the newton solver for an unknown x */
// has to store f(x) in fdf_[0] and df/dx in fdf[1]

Vector molality_;
 Vector charge_;

static constexpr Scalar ac_ = 2000; /* [1/dm] from Anozie Ebigbo 2011 // TODO Check for value, fittet parameter!!!!!!!!!!!!*/
static constexpr Scalar kdiss1_ = 8.9e-3; // [kgH2O/dm²s] from Anozie (17)
static constexpr Scalar kdiss2_ = 6.5e-9; // [mol/dm²s] from Anozie (17)
// static constexpr Scalar kprec_ = 1.5e-12; // [mol/dm²s] from Anozie (29)change to see the mass fraction effect..
//static constexpr Scalar kprec_ = 1.9e-10;//increased the value to see the precip. according to reality..
//static constexpr Scalar kprec_ = 2.5e-10;//previous results good but still doing for better results..
static constexpr Scalar kprec_ = 9e-3;//9e-4;  //9e-5 //5e-7;	// 2.1e-9 //further increased as the mass fractions of the components a bit higher.
static constexpr Scalar ndiss_ = 1.0; // TODO ask Anozie from where
//static constexpr Scalar nprec_ = 3.27; // [-] from Anozie (29)
static constexpr Scalar nprec_ =  1.2;//1.25; //1.3   //3.25;// 3.15. //just for checking from the previous value...
static constexpr Scalar Asw0_ = 500; // [1/dm]estimated from Anozie A/V

public:
Scalar kprec() const
{ return kprec_;}
Scalar nprec() const
{ return nprec_;}
Scalar Asw0() const
{ return Asw0_;}

static constexpr Scalar nSw_ = 1;

};


} // end namepace

#endif // KCLCHEM_HH
