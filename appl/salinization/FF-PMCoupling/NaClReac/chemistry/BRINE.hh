/*
 * BrineReaction.hh
 *
 *  Created on: 11.11.2013
 *      Author: saideep
 */

#ifndef BRINE_HH_
#define BRINE_HH_


// Brine Salt Precipitation Chemistry...

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>	       // for the specific Na,Cl component info.
#include <dumux/material/fluidsystems/naclfluidsystem.hh>  //properties of the NaCl fluid system as whole.
#include <dumux/implicit/2pncmin/2pncminproperties.hh>
#include <cmath>
#include <iostream>

namespace Dumux
{
/*!
* \The equilibrium chemistry is calculated in this class. The function to calculateEquilbriumChemistry is used to control the Newton Solver "newton1D". The chemical functions and derivations are implemented in the private part of
class. */

/*SET_PROP(Brine, Fluid)
		{
		private:
	typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
		public:
	typedef Dumux::LiquidPhase<Scalar, Dumux::SimpleH2O<Scalar> > type;
		};*/

template <class TypeTag>
class BrineReaction
{																	//private defined
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

typedef BrineReaction<TypeTag> ThisType;

public:
  
static const int wPhaseIdx = FluidSystem::wPhaseIdx;
static const int nPhaseIdx = FluidSystem::nPhaseIdx;
static const int wCompIdx = FluidSystem::wCompIdx;
static const int nCompIdx = FluidSystem::nCompIdx;
static const int H2OIdx = FluidSystem::H2OIdx;
static const int NaIdx = FluidSystem::NaIdx;
static const int ClIdx = FluidSystem::ClIdx;
static const int sPhaseIdx = FluidSystem::sPhaseIdx;  //for the precipitation
static const int precipNaClIdx = FluidSystem::numComponents;  //for the precipitation


static const int numComponents = FluidSystem::numComponents;
static const int numMajorComponents = FluidSystem::numMajorComponents;
static const int numSecComponents = FluidSystem::numSecComponents;
static const int numTotComponents = numComponents + numSecComponents;
static const int numPhases = FluidSystem::numPhases;


typedef Dune::FieldVector<Scalar, 3> Vector;
typedef Dune::FieldVector<Scalar, 2> SolVector;
typedef Dune::FieldVector<Scalar, numTotComponents> CompVector;

//    Return equlibrium constant for dissolution reaction:
//    Na(s)+ Cl(g) <--> NaCl(s)

static Scalar solubilityProduct()
{
	return 28.90146303; // mol²/kg²
	/* The solubility product here is defined with the conversion from [mol²/l²] to
	[mol²/kg²] with the conversion as follows:
	Ksp = 38.68 mol²/l² and by density(1165kg/m³) = mass / volume (10^-3 m³). So, dividing the initial
	Ksp with {38.6890/(1.165*1.165)}  */ //return 38.689082; //changed according to the temperature..			//return(37.8594); 			// 6.15*6.15 moles per litre.(360g/lt).
}

//static Scalar massFracToMolality(const Scalar massFracX, const Scalar molarMassX, const Scalar massFracC, const Scalar massFracSalinity)
//{
//Scalar molalityX = (massFracX)/(molarMassX*(1- massFracX - massFracSalinity));
//return molalityX; 					// from Wiki....
//}

/*!* Returns the mass fraction of a component x (kg x / kg solution) for a given molality fraction (mol x / mol solution).* The salinity and the mole Fraction of CO2 are considered* */

//static Scalar molalityToMassFrac(Scalar molalityX, Scalar molarMassX, Scalar massFracSalinity)
//{
//Scalar massFracX = (molalityX * molarMassX * (1- massFracSalinity)) / (1+(molalityX*molarMassX));
//return massFracX; 			// modification of the above formula...
//}


static Scalar moleFracToMolality(Scalar moleFracX, Scalar moleFracSalinity)
{
Scalar molalityX = (moleFracX) / ((1-/*moleFracX*/ moleFracSalinity)* FluidSystem::molarMass(H2OIdx));
return molalityX;
}   	/*commented moleFracX because it is subtracting the secondary Na, Cl component twice*/



//static Scalar molalityToMoleFrac(Scalar molalityX, Scalar moleFracSalinity)
//{
//Scalar moleFracX = (molalityX * FluidSystem::molarMass(H2OIdx) * (1 - moleFracSalinity))/(1+(molalityX * FluidSystem::molarMass(H2OIdx)));
//return moleFracX; 		//modification of the above formula...
//}


static Scalar ionicStrength(const VolumeVariables &volVars)
{
	Scalar ionicStrengthnacl_=0;
	for (int compIdx= numMajorComponents; compIdx < numComponents; ++compIdx)
	{
		Scalar molality = moleFracToMolality(volVars.moleFraction(wPhaseIdx, compIdx), volVars.moleFracSalinity());
		ionicStrengthnacl_ +=  molality * FluidSystem::charge(compIdx)* FluidSystem::charge(compIdx);
		ionicStrengthnacl_ *= 0.5;
	}
	return (ionicStrengthnacl_);
}

/*static Scalar ionicStrengthNa()
{
Scalar ionicStrength = 0.0;
//Scalar molality = 500;
//Scalar charge = 1;
ionicStrength += molality_ * charge_ * charge_;
ionicStrength *= 0.5;
return ionicStrength;
}
// Commented as the Ionic Strength (I) is'nt a component property but it is a phase property.
 *
static Scalar ionicStrengthCl()
{

Scalar ionicStrength = 0.0;
//Scalar molality=500;
//Scalar charge =1;
ionicStrength += molality_ * charge_ * charge_;
ionicStrength *= 0.5;
return ionicStrength;
}*/


//Calculates the activity with a modified Debye-Hückel equation after Parkhurst (1990) for ionic strengths up to 2.//

static Scalar activityCoefficient(Scalar ionicStrengthnacl_, Scalar temperatureK, int compIdx)
{
if (ionicStrengthnacl_<0)
{ 							//For the Newton iteration.
ionicStrengthnacl_ = 0;
}
//
///* Scalar temperatureC = temperatureK - 273.15; //Temperature in °C
//Finding density relation:Scalar d=1-pow((temperatureC – 3.9863),2)*(temperatureC + 288.9414)/(508929.2*(temperatureC + 68.12963))+0.011445*exp(-374.3 / temperatureC);
//// dielectric constant:
//Scalar eps=2727.586+0.6224107*temperatureKconst-466.9151*log(temperatureK)-52000.87/temperatureK;
////A, B are temperature dependent parameters for the Debye-Hückel equation
//// Scalar A = 1.82483e6 * sqrt(d)/pow(eps*temperatureK, 1.5);
//// Scalar B = 50.2916*sqrt(d)/pow(eps*temperatureK, 0.5);*/
//
Scalar A = 0.5215;
Scalar B = 0.33e8;


Scalar charge = FluidSystem::charge(compIdx);
Scalar ai = FluidSystem::ai(compIdx);
Scalar bi = FluidSystem::bi(compIdx);
//
//
//// The actual modified Debye Hückel equation that is being applied.
//
//
Scalar logActivityCoefficient = -A*pow(charge, 2)*sqrt(ionicStrengthnacl_)/(1 + (B*ai*sqrt(ionicStrengthnacl_)))
                        + (bi*ionicStrengthnacl_);
						/*commented to check if the molality
                        is only having the effect and activity has no impact over the change
                        in values*/

return pow(10, logActivityCoefficient);
}

// if (compIdx==NaIdx) {
//Scalar logActivityCoefficientNa = -A*pow(charge, 2.)*sqrt(ionicStrengthnacl_)/(1 + B*ai*sqrt(ionicStrengthnacl_))
//                        + bi*ionicStrengthnacl_;
//
//return pow(10, logActivityCoefficientNa);
//}
//
// for (int compIdx= numMajorComponents; compIdx <= numComponents; ++compIdx)
// {
//	 Scalar logActivityCoefficient = 0;
//	 logActivityCoefficient += -A*pow(charge, 2.)*sqrt(ionicStrength)/(1 + B*ai*sqrt(ionicStrength))
//                                     + bi*ionicStrength;
//	 return pow(10, logActivityCoefficient);
// }
//
//
//if (compIdx==ClIdx) {
//Scalar logActivityCoefficientCl = -A*pow(charge, 2.)*sqrt(ionicStrength)/(1 + B*ai*sqrt(ionicStrength))
//                        + bi*ionicStrength;
//
//return pow(10, logActivityCoefficientCl);
//					}	// return 1 incase of error.
//}


void reactionSource(PrimaryVariables &q,
		const VolumeVariables &volVars, Scalar kprec,  Scalar nprec, Scalar theta, Scalar nSw)
{

q = 0;

//define and compute some parameters for simplicity:

Scalar xlSalinity = volVars.moleFracSalinity();
Scalar initialPorosity = volVars.InitialPorosity();
Scalar Sw = volVars.saturation(wPhaseIdx);

Scalar VolFracPrecipitation = std::min(initialPorosity - 0.101, volVars.solidity(sPhaseIdx));
if (VolFracPrecipitation < 1e-20)
VolFracPrecipitation = 0;

Scalar mNa = moleFracToMolality(volVars.moleFraction(wPhaseIdx,NaIdx), xlSalinity)
		    * activityCoefficient(ionicStrength(volVars),volVars.temperature(), NaIdx); //[mol_sodium/kg_H2O]
if (mNa < 1e-20)
mNa = 0;

Scalar mCl = moleFracToMolality(volVars.moleFraction(wPhaseIdx,ClIdx), xlSalinity)
		   * activityCoefficient(ionicStrength(volVars),volVars.temperature(), ClIdx);  //[mol_Cl/kg_H2O]
if (mCl < 1e-20)
mCl = 0;

Scalar Ksp =  solubilityProduct();
omega_ = (mNa * mCl) / Ksp;

Scalar Asw = Asw0_ * pow(1-((VolFracPrecipitation)/initialPorosity),(2.0/3.0));
Asw *= pow(Sw,nSw);

Scalar rdiss = 0;

Scalar rprec = 0;

if (omega_ >= 1)
{
rdiss = 0;
rprec = kprec * Asw * pow(pow(omega_,theta) - 1 , nprec);//[mol/dm³s]
rprec *= 1000; // rprec [mol/m³s]
}

Scalar kdiss = kprec*1e-3;
Scalar ndiss = nprec;


if (omega_ < 1 && VolFracPrecipitation != 0)
{
rdiss = kdiss * Asw * pow(1 - omega_, ndiss); //[mol/dm³s]
rdiss *= 1000; // rdiss [mol/m³s]
rprec = 0;
}

q[wCompIdx] = 0.0; //IN ÄNDERUNG DES MOLENB=RUCHS UMRECHNEN-Water as const.
q[nCompIdx] = 0.0;//Air % inc. When dissolution and dec when precip.
q[NaIdx] = (-rprec + rdiss);
q[ClIdx] = (-rprec + rdiss);
q[precipNaClIdx] = (rprec - rdiss);
}
private:

Vector molality_;
Vector charge_;

static constexpr Scalar Asw0_ = 500; // [1/dm]estimated from Anozie A/V
static constexpr Scalar nSw_ = 1;
Scalar omega_;

public:
    
Scalar omega(Scalar sPhaseIdx) const
{ if (sPhaseIdx == 0)
    return omega_;
  else
    DUNE_THROW(Dune::InvalidStateException, "Invalid solid phase index " << sPhaseIdx);
}

};


} // end namepace


#endif /* BRINE_HH_ */
