// $Id: 2p2cvolumevariables.hh 5151 2011-02-01 14:22:03Z lauser $
/*****************************************************************************
 *   Copyright (C) 2008,2009 by Vishal Jambhekar,
 * 								Alexzander Kissinger,
 * 								Klaus Mosthaf,                               *
 *                              Andreas Lauser,                              *
 *                              Bernd Flemisch                               *
 *   Institute of Hydraulic Engineering                                      *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase, n-component mineralisation model.
 */
#ifndef DUMUX_2PNCMINNI_VOLUME_VARIABLES_HH
#define DUMUX_2PNCMINNI_VOLUME_VARIABLES_HH

#include <dumux/implicit/2pncmin/2pncminvolumevariables.hh>

namespace Dumux
{

/*!
 * \ingroup TwoPNCMinNIModel
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the two-phase, n-component non-isothermal mineralisation model.
 */
template <class TypeTag>
class TwoPNCMinNIVolumeVariables : public TwoPNCMinVolumeVariables<TypeTag>
{
    typedef TwoPNCMinVolumeVariables<TypeTag> parentType;
    typedef TwoPNCMinNIVolumeVariables<TypeTag> thisType;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(VolumeVariables)) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Scalar)) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Grid)) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(GridView)) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(Problem)) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FVElementGeometry)) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(PrimaryVariables)) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(FluidSystem)) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(MaterialLaw)) MaterialLaw;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(MaterialLawParams)) MaterialLawParams;
    typedef typename GET_PROP_TYPE(TypeTag, PTAG(TwoPNCMinIndices)) Indices;
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename parentType::FluidState FluidState;
    enum {
    	  temperatureIdx = Indices::temperatureIdx,
    	  numPhases = FluidSystem::numPhases,
		  useSalinity = GET_PROP_VALUE(TypeTag, useSalinity)
         };

public:
        /*!
         * \brief Returns the total internal energy of a phase in the
         *        sub-control volume.
         *
         * \param phaseIdx The phase index
         */
        Scalar internalEnergy(int phaseIdx) const
        { return this->fluidState_.internalEnergy(phaseIdx); };

        /*!
         * \brief Returns the total enthalpy of a phase in the sub-control
         *        volume.
         *
         * \param phaseIdx The phase index
         */
        Scalar enthalpy(int phaseIdx) const
        { return this->fluidState_.enthalpy(phaseIdx); };

        /*!
         * \brief Returns the total heat capacity \f$\mathrm{[J/(K*m^3]}\f$ of the rock matrix in
         *        the sub-control volume.
         */
        Scalar heatCapacity(int phaseIdx) const
        {
	  if (phaseIdx < numPhases)
		  return heatCapacity_;
#if SALINIZATION
	  else if (phaseIdx >= numPhases)
		  return std::abs((parentType::InitialPorosity() - parentType::porosity()))
			  * parentType::density(phaseIdx)
			  * FluidSystem::saltSpecificHeatCapacity(phaseIdx);
#endif
	  else
		  DUNE_THROW(Dune::InvalidStateException, "Invalid phase index or useSaliniy tag set to false " << phaseIdx);
        };
	/*!
	   * \brief Returns the thermal conductivity \f$\mathrm{[W/(m*K)]}\f$ of the fluid phase in
	   *        the sub-control volume.
	   */
	  Scalar thermalConductivity(const int phaseIdx) const
	  { return FluidSystem::thermalConductivity(this->fluidState_, phaseIdx); };


protected:
        // this method gets called by the parent class. since this method
        // is protected, we are friends with our parent..
        friend class TwoPNCVolumeVariables<TypeTag>;
		friend class TwoPNCMinVolumeVariables<TypeTag>;

        static Scalar temperature_(const PrimaryVariables &primaryVars,
                                   const Problem& problem,
                                   const Element &element,
                                   const FVElementGeometry &fvGeometry,
                                   const int scvIdx)
        {
            return primaryVars[temperatureIdx];
        }

        template<class ParameterCache>
        static Scalar enthalpy_(const FluidState& fluidState,
                                const ParameterCache& paramCache,
                                const int phaseIdx)
        {
            return FluidSystem::enthalpy(fluidState, paramCache, phaseIdx);
        }
        /*!
         * \brief Update all quantities for a given control volume.
         *
         * \param priVars The solution primary variables
         * \param problem The problem
         * \param element The element
         * \param fvGeometry Evaluate function with solution of current or previous time step
         * \param scvIdx The local index of the SCV (sub-control volume)
         * \param isOldSol Evaluate function with solution of current or previous time step
         */
        void updateEnergy_(const PrimaryVariables &priVars,
                           const Problem &problem,
                           const Element &element,
                           const FVElementGeometry &fvGeometry,
                           const int scvIdx,
                           bool isOldSol)
        {
            // copmute and set the heat capacity of the solid phase
            heatCapacity_ = problem.spatialParams().heatCapacity(element, fvGeometry, scvIdx);
            Valgrind::CheckDefined(heatCapacity_);
        };

private:
        Scalar heatCapacity_;
    };
    } // end namespace

    #endif
