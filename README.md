Examples from Jambhekar et al. 2016
===================================

Jambhekar, V. A., Mejri, E., Schröder, N., Helmig, R., Shokri, N. Transport in Porous Media, 2016
[Kinetic Approach to Model Reactive Transport and Mixed Salt Precipitation in a Coupled Free-Flow--Porous-Media System](http://dx.doi.org/10.1007/s11242-016-0665-3)

For your convenience please find the [BibTex entry here](https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015b/raw/master/jambhekar2016.bib).

Installation
===================================

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above. For the basic
dependencies see dune-project.org.

The easiest way is to use the `installJambhekar2015b.sh` in this folder.
You might want to look at it before execution [here](https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015b/raw/master/installJambhekar2015b.sh). Create a new folder containing the script and execute it.
You can copy the following to a terminal:
```bash
mkdir -p Jambhekar2015b && cd Jambhekar2015b
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Jambhekar2015b/raw/master/installJambhekar2015b.sh
chmod +x installJambhekar2015b.sh && ./installJambhekar2015b.sh
```
It will install this module will take care of most dependencies.
For the basic dependencies see dune-project.org and at the end of this README.

How to run the examples
===================================

Once built please find the executables in the folder
`Jambhekar2015b/build-cmake/appl/salinization/FF-PMCoupling/NaClReac/`
to run the example
```bash
$ cd Jambhekar2015b/build-cmake/appl/salinization/FF-PMCoupling/NaClReac/
$ ./test_2cnistokes2pncnireac
```

Dependencies Info
===================================

When this module was created, the original module dumux-devel was using
the following list of DUNE modules and third-party libraries.
BEWARE: This does not necessarily mean that the applications in this
extracted module actually depend on all these components.

* dumux...................: version 2.6
* dune-common.............: version 2.3.1
* dune-geometry...........: version 2.3.1
* dune-grid...............: version 2.3.1
* dune-istl...............: version 2.3.1
* dune-localfunctions.....: version 2.3.1
* dune-multidomain........: version 2.0
* dune-multidomaingrid....: version 2.3.0
* dune-pdelab.............: version 2.0.0
* dune-typetree...........: version 2.3.1
* MPI.....................: no
* PETSc...................: no
* ParMETIS................: no
* SuperLU-DIST............: no
* SuperLU.................: yes (version 4.3 or newer)
* UG......................: yes (sequential)
* UMFPACK.................: no
* psurface................: no
