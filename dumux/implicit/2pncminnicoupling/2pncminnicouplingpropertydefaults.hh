/*****************************************************************************
 *   Copyright (C) 2008-2009 by Klaus Mosthaf                                *
 *   Copyright (C) 2008-2010 by Andreas Lauser                               *
 *   Copyright (C) 2008-2009 by Bernd Flemisch                               *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTwoCNIModel
 */
/*!
 * \file
 *
 * \brief Defines additional default values for the properties
 *        required for the coupling of the 2p2cni box model
  */
#ifndef DUMUX_2PNCMINNI_COUPLING_PROPERTY_DEFAULTS_HH
#define DUMUX_2PNCMINNI_COUPLING_PROPERTY_DEFAULTS_HH

#include <dumux/implicit/2pncminni/2pncminnipropertydefaults.hh>

#include "2pncminnicouplingmodel.hh"
#include "2pncminnicouplinglocalresidual.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Property values
//////////////////////////////////////////////////////////////////
//! Use the 2p2cni local jacobian operator for the 2p2cniCoupling model
//! The indices required by the non-isothermal 2p2c model
SET_TYPE_PROP(BoxTwoPNCMinNICoupling,
              LocalResidual,
              TwoPNCMinNICouplingLocalResidual<TypeTag>);

//! the Model property
SET_TYPE_PROP(BoxTwoPNCMinNICoupling, Model, TwoPNCMinNICouplingModel<TypeTag>);
}
}
#endif
