// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   Copyright (C) 2012 by Klaus Mosthaf                                     *
 *   Copyright (C) 2009 by Andreas Lauser                                    *
 *   Institute for Modelling Hydraulic and Environmental Systems             *
 *   University of Stuttgart, Germany                                        *
 *   email: <givenname>.<name>@iws.uni-stuttgart.de                          *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Porous-medium subproblem with coupling at the top boundary.
 */
#ifndef DUMUX_2PNCNIREACSUB_PROBLEM_HH
#define DUMUX_2PNCNIREACSUB_PROBLEM_HH

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dumux/implicit/common/implicitporousmediaproblem.hh>
#include <dumux/implicit/2pncminnicoupling/2pncminnicouplingmodel.hh>
#include <dumux/modelcoupling/common/subdomainpropertydefaults.hh>
#include <dumux/modelcoupling/common/multidomainboxlocaloperator.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include "chemistry/BRINE.hh"

namespace Dumux
{
template <class TypeTag>
class TwoPNCNIReacSubProblem;

namespace Properties
{
NEW_TYPE_TAG(TwoPNCNIReacSubProblem,
             INHERITS_FROM(BoxTwoPNCMinNICoupling, SubDomain, TwoCNIStokesTwoPNCNIReacSpatialParams));

// Set the problem property
SET_TYPE_PROP(TwoPNCNIReacSubProblem, Problem, Dumux::TwoPNCNIReacSubProblem<TTAG(TwoPNCNIReacSubProblem)>);

SET_INT_PROP(TwoPNCNIReacSubProblem, Formulation, TwoPNCFormulation::pgSl);


// the gas component balance (air) is replaced by the total mass balance
SET_PROP(TwoPNCNIReacSubProblem, ReplaceCompEqIdx)
{
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    static const int value = Indices::contiNEqIdx;
};
// the fluidsystem is set in the coupled problem
SET_PROP(TwoPNCNIReacSubProblem, FluidSystem)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;
    typedef typename GET_PROP_TYPE(CoupledTypeTag, FluidSystem) FluidSystem;
public:
    typedef FluidSystem type;
};

/*describe the insertion of chemistry into the code from the BRINE.hh code*/
SET_PROP(TwoPNCNIReacSubProblem, Chemistry)
{
typedef Dumux::BrineReaction<TypeTag> type;
};

// enable/disable velocity output
SET_BOOL_PROP(TwoPNCNIReacSubProblem, VtkAddVelocity, true);

// Enable gravity
SET_BOOL_PROP(TwoPNCNIReacSubProblem, ProblemEnableGravity, true);
}

template <class TypeTag = TTAG(TwoPNCNIReacSubProblem)>
class TwoPNCNIReacSubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    typedef TwoPNCNIReacSubProblem<TypeTag> ThisType;
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

	/* defining the usage of chemistry*/
    typedef typename GET_PROP_TYPE(TypeTag, Chemistry) Chemistry;
    
    // the type tag of the coupled problem
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) CoupledTypeTag;

    // Declaration of fluid system
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
	typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    enum { 	// number of equations
    		numEq = GET_PROP_VALUE(TypeTag, NumEq),

    		//Indices of the components
			wCompIdx = FluidSystem::H2OIdx,
			nCompIdx = FluidSystem::AirIdx,
			NaIdx = FluidSystem::NaIdx,
			ClIdx = FluidSystem::ClIdx,
			
			//Indices of the phases
			wPhaseIdx = FluidSystem::wPhaseIdx,
			nPhaseIdx = FluidSystem::nPhaseIdx,
			sPhaseIdx = FluidSystem::sPhaseIdx,

			// the equation indices
			conti0EqIdx 	  = Indices::conti0EqIdx,
			contiWEqIdx       = conti0EqIdx + FluidSystem::H2OIdx,
			contiTotalMassIdx = conti0EqIdx + FluidSystem::AirIdx, // TODO: conti of the total mass in the PM.
			contiNaEqIdx    = conti0EqIdx + FluidSystem::NaIdx,
			contiClEqIdx    = conti0EqIdx + FluidSystem::ClIdx, 
			precipNaClEqIdx   = conti0EqIdx + FluidSystem::numComponents,
			energyEqIdx 	  = conti0EqIdx + Indices::energyEqIdx,

			// the indices of the primary variables
			pressureIdx   = Indices::pressureIdx,
			switchIdx     = Indices::switchIdx,
			xlNaIdx     = FluidSystem::NaIdx,
			xlClIdx	    = FluidSystem::ClIdx, 
			precipNaClIdx = FluidSystem::numComponents,
			temperatureIdx = Indices::temperatureIdx,


			// the indices for the phase presence
			wPhaseOnly = Indices::wPhaseOnly,
			nPhaseOnly = Indices::nPhaseOnly,
			bothPhases = Indices::bothPhases,

			// grid and world dimension
			dim 	 = GridView::dimension,
			dimWorld = GridView::dimensionworld
    	};

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    TwoPNCNIReacSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
    	try
    	{
            Scalar noDarcyX 	= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, NoDarcyX);
            Scalar xMin 		= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, XMin);
            bboxMin_[0] 		= std::max(xMin,noDarcyX);
            bboxMax_[0] 		= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, XMax);
            bboxMin_[1] 		= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, YMin);
            bboxMax_[1] 		= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePos);
            runUpDistanceX_ 	= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, RunUpDistanceX); // first part of the interface without coupling
            xMaterialInterface_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, MaterialInterfaceX);
            initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);
            refTemperature_ 	= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperaturePM);
            refTemperatureFF_ 	= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
            refPressure_ 		= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefPressurePM);
            
            massFracNa_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, massFracNa);
            massFracCl_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, massFracCl);
            minPorosity_     = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, minPorosity);
            initialSw_ 		= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, InitialSw);
            scalingFactor_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, scalingFactor);
            freqMassOutput_ 	= GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);
            storageLastTimestep_ = Scalar(0);
            lastMassOutputTime_ = Scalar(0);
            episodeLength_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
            nprec_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, precCoefficient, n_prec);
            kprec_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, precCoefficient, k_prec);
            theta_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, precCoefficient, theta);
            nSw_   = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, precCoefficient, nSw);
            outfile.open("evaporationData.out");
            outfile << "   time;  evaporationRate;  cumulativeWaterLoss;  CumulativeSaltLoss(Na);  CumulativeSaltLoss (Cl);  CumulativeSaltAccumilation" << std::endl;
        
            episodeData.open("episodeData.txt");
            episodeData << " Time ; Salt (gms) " << std::endl;   
        }
    	catch (Dumux::ParameterException &e) {
    	    std::cerr << e << ". Abort!\n";
    	    exit(1) ;
    	}
    	catch (...) {
    	     std::cerr << "Unknown exception thrown!\n";
    	     exit(1);
    	}
    }

    ~TwoPNCNIReacSubProblem()
    {
        outfile.close();
	episodeData.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string &name() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Vtk, NamePM); }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        // set the initial condition of the model
        ParentType::init();

        this->model().globalStorage(storageLastTimestep_);
    
        const int blModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, FreeFlow, UseBoundaryLayerModel);
        std::cout << "Using boundary layer model " << blModel << std::endl;
    }

	/*bool shouldWriteOutput() const		// change this for getting the output later!! Donot forget.
	{
	return (this-> timeManager().episodeWillBeOver());
	}*/

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex for which the boundary type is set
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        Scalar time = this->timeManager().time();
        
        values.setAllNeumann();

        if (onLowerBoundary_(globalPos))
        {
        	values.setDirichlet(temperatureIdx, energyEqIdx);
        }

        if (onUpperBoundary_(globalPos))
        {
            if (time > initializationTime_)
            {
                if (globalPos[0] > runUpDistanceX_ - eps_)
                {
                    values.setCouplingInflow(contiWEqIdx);
                    values.setCouplingInflow(contiTotalMassIdx);
                    values.setCouplingInflow(energyEqIdx);
                    values.setNeumann(xlNaIdx);
                    values.setNeumann(xlClIdx);
                    values.setNeumann(precipNaClIdx);
                }
                else
                    values.setAllNeumann();
            }
            else
            {
                values.setAllNeumann();
            }
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex for which the boundary type is set
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        initial_(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param is The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
    	values = 0.;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * of a component is generated or annihilate per volume
     * unit. Positive values mean that mass is created, negative ones
     * mean that it vanishes.
     */

	void solDependentSource(PrimaryVariables & q,
				const Element & element,
				const FVElementGeometry &fvGeometry,
				int scvIdx,
                const ElementVolumeVariables & elemVolVars) const
{
    const VolumeVariables &volVars = elemVolVars[scvIdx];
    q =0;
    Chemistry chemistry;
    chemistry.reactionSource(q, volVars, kprec_, nprec_, theta_, nSw_);
}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param scvIdx The local vertex index
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);
        values = Scalar(0);
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param vert The vertex
     * \param globalIdx The index of the global vertex
     * \param globalPos The global position
     */
    int initialPhasePresence(const Vertex &vert,
                             const int &globalIdx,
                             const GlobalPosition &globalPos) const
    {
        return bothPhases;
    }

    void preTimeStep()
    {}

    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        this->model().globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqMassOutput_ == 0
                    || this->timeManager().episodeWillBeOver())
            {
		PrimaryVariables evaporationRate(0.);
                evaporationRate = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                evaporationRate /= (time - lastMassOutputTime_);
                // 2d: interface length has to be accounted for
                // in order to obtain mol/m²s
                evaporationRate /= (bboxMax_[dim-2]-bboxMin_[dim-2]);

                // in order to obtain kg/m²s multiply with molar mass and writing into .out file

                if (this->timeManager().time() != 0.)
                    outfile << time <<
		    "; " << evaporationRate[contiWEqIdx] * FluidSystem::molarMass(wCompIdx) << 
		    "; " << (storage[contiWEqIdx] * FluidSystem::molarMass(wCompIdx))<< 
		    "; " << (storage[NaIdx] * FluidSystem::molarMass(NaIdx))<<
		    "; " << (storage[ClIdx] * FluidSystem::molarMass(ClIdx))<<
		    "; " << (storage[precipNaClEqIdx] *((FluidSystem::molarMass(NaIdx))+ FluidSystem::molarMass(ClIdx)))<< std::endl;
                storageLastTimestep_ = storage;
                lastMassOutputTime_ = time;             
            }
        }
    }

void episodeEnd()
{

PrimaryVariables storage;
this->model().globalStorage(storage);
const Scalar time = this->timeManager().time() /*+  this->timeManager().timeStepSize()*/;
	this->timeManager().startNextEpisode(episodeLength_); /* that is i am setting the episode length to 1 hour for 20 hours.*/
	episodeData <<  time << " ; " << (storage[precipNaClEqIdx]*(FluidSystem::molarMass(NaIdx)+FluidSystem::molarMass(ClIdx))) *scalingFactor_ << std::endl; // Scaled the domain to the experimental domain size
}
    /*!
     * \brief Determine if we are on a corner of the grid
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;
    }

    // required in case of mortar coupling
    // otherwise it should return false
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    { return false; }

    // \}
private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[pressureIdx]    = refPressure_ + 1000.*this->gravity()[1]*(globalPos[1]-bboxMax_[1]);
        values[switchIdx]      = initialSw_;
        values[xlNaIdx]      = massTomoleFracNa_(massFracNa_);    //mass frac to mole fraction consversion.
        values[xlClIdx]	     = massTomoleFracCl_(massFracCl_);	  //convsrsion from mass fraction specified to mole frac. 
        values[precipNaClIdx]  = 0.0; // [kg/m^3]
        values[temperatureIdx] = refTemperature_;
           if (globalPos[dim -1] > 0.10 && globalPos[dim -1] < 0.15 && globalPos[dim -2] > 0.10 &&  globalPos[dim -2] < 0.15)
                    values[precipNaClIdx] = 0.20; // [kg/m^3]
    }

   static Scalar massTomoleFracNa_(Scalar XNa)
   {
	   const Scalar Mw = 18.015e-3; /* molecular weight of water [kg/mol] */
	   const Scalar Ms = 22.9898e-3; /* molecular weight of NaCl  [kg/mol] */

	   const Scalar X_Na = XNa;
	   /* XlNaCl: conversion from mass fraction to mol fraction */
	   const Scalar xlNa = -Mw * X_Na / ((Ms - Mw) * X_Na - Ms);
	   return (xlNa);
   }

  static Scalar massTomoleFracCl_(Scalar XCl)
    {
        const Scalar Mw = 18.015e-3;
        const Scalar Ms = 35.453e-3;

        const Scalar X_Cl = XCl;
        const Scalar xlCl = -Mw * X_Cl / ((Ms - Mw) * X_Cl - Ms);
        return (xlCl);
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bboxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bboxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bboxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bboxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bboxMin_;
    GlobalPosition bboxMax_;
    Scalar xMaterialInterface_;

    int freqMassOutput_;

    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;

    Scalar refTemperature_;
	Scalar refTemperatureFF_;
    Scalar refPressure_;
    Scalar initialSw_;
    Scalar massFracNa_;
    Scalar massFracCl_;
    Scalar minPorosity_;
    Scalar episodeLength_;
    Scalar runUpDistanceX_;
    Scalar initializationTime_;
    Scalar kprec_;
    Scalar nprec_;
    Scalar theta_;
    Scalar nSw_;
    Scalar scalingFactor_;
    std::ofstream outfile;
    std::ofstream episodeData;    
};
} //end namespace

#endif
