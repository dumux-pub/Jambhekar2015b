#ifndef NACLKCLCHEM_HH
#define NACLKCLCHEM_HH

#include <dumux/common/exceptions.hh>
#include <dumux/material/components/component.hh>
/* need the components of Na / Cl / K*/
#include <dumux/material/fluidsystems/naclkfluidsystem.hh>
/* properties of the NaClK fluid system as whole. */
#include <dumux/implicit/2pncmin/2pncminproperties.hh>
#include <cmath>
/* for implementing the math operations. Essential for this.*/
#include <iostream>
/* for showing output onto the screen.*/

namespace Dumux
{
template <class TypeTag>
class NaClK
{
typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;

typedef NaClK<TypeTag> ThisType;

public:
/* use try and catch if necessary at this portion.*/
static const int wPhaseIdx = FluidSystem::wPhaseIdx;
static const int nPhaseIdx = FluidSystem::nPhaseIdx;
static const int wCompIdx = FluidSystem::wCompIdx;
static const int nCompIdx = FluidSystem::nCompIdx;
static const int H2OIdx = FluidSystem::H2OIdx;
static const int NaIdx = FluidSystem::NaIdx;
static const int ClIdx = FluidSystem::ClIdx;
static const int KIdx = FluidSystem::KIdx;
static const int NaClPhaseIdx = FluidSystem::NaClPhaseIdx;  //for the precipitated NaCl
static const int KClPhaseIdx = FluidSystem::KClPhaseIdx;  //for the precipitated KCl


static const int numComponents = FluidSystem::numComponents;
static const int numMajorComponents = FluidSystem::numMajorComponents;
static const int numSecComponents = FluidSystem::numSecComponents;
static const int numTotComponents = numComponents + numSecComponents;
static const int numPhases = FluidSystem::numPhases;

typedef Dune::FieldVector<Scalar, 3> Vector;
typedef Dune::FieldVector<Scalar, 2> SolVector;
typedef Dune::FieldVector<Scalar, numTotComponents> CompVector;

/* Na(s) + Cl(s) → NaCl(s) &
    K(s) + Cl(s)  → Kcl(s)*/
/* solubility product calculated as follows:
intial solubility = 360gms/lt → forms 360/58.5 → 6.1538mol/lt of Na and Cl that is
6.1538* 6.1538 = 37.869823mol²/lt² →(based on the density, 1.150) 29.76714 and 17.63189 mol²/kg².  */

static Scalar solubilityProductNaCl()
{
return (29.76714);
/* value in mol²/kg²*/
}

static Scalar solubilityProductKCl()
{
return (17.63189);
/* value in mol²/kg²*/
}

/* function definition for the conversion from molefraction to molality by standard formula.
Used later.*/
static Scalar moleFracToMolality (Scalar moleFracX, Scalar moleFracSalinity)
{
Scalar molalityX = (moleFracX) / ((1-/*moleFracX*/ moleFracSalinity)* FluidSystem::molarMass(H2OIdx));
return (molalityX);
}

static Scalar ionicStrength(const VolumeVariables & volVars)
{
Scalar ionicStrengthnaclk_ = 0;
    for (int compIdx = numMajorComponents; compIdx<numComponents; ++compIdx)
    {
    Scalar molality =  moleFracToMolality(volVars.moleFraction(wPhaseIdx, compIdx), volVars.moleFracSalinity());
    ionicStrengthnaclk_ += molality*FluidSystem::charge(compIdx)* FluidSystem::charge(compIdx);
    ionicStrengthnaclk_ *= 0.5;
return (ionicStrengthnaclk_);
    }
}

/* to calculate the activity coefficient of the components*/
static Scalar activityCoefficient (Scalar ionicStrengthNaClK_, Scalar temperatureK, int compIdx)
{
    if (ionicStrengthNaClK_ <0)
    {
    ionicStrengthNaClK_ = 0; /*in case of convergence modifications set it upto 0.*/
    }
Scalar A = 0.5215;
Scalar B = 0.33e8;

Scalar charge = FluidSystem::charge(compIdx);
Scalar ai = FluidSystem::ai(compIdx);
Scalar bi = FluidSystem::bi(compIdx);

/*implementing the main Debye-Hückel equation into the system.*/

Scalar logActivityCoefficient = -A*pow(charge, 2)*sqrt(ionicStrengthnacl_)/(1 + (B*ai*sqrt(ionicStrengthnacl_))) + (bi*ionicStrengthnacl_);

return (pow(10, logActivityCoefficient));
}
void reactionSource(PrimaryVariables & q,
                  const VolumeVariables & volVars)
{
q = 0;
Scalar xlSalinity = volVars.moleFracSalinity();
Scalar initialPorosity = volVars.InitialPorosity();
Scalar Sw = volVars.saturation(wPhaseIdx);
Scalar VolFracPrecipitation_NaCl = std::min(initialPorosity - 0.101, volVars.solidity(NaClPhaseIdx));
Scalar VolFracPrecipitation_KCl = std::min(initialPorosity - 0.101, volVars.solidity(KClPhaseIdx));
 /* limiting the quantities of solidity being formed.*/

if (VolFracPrecipitation_NaCl < 1e-20)
VolFracPrecipitation_NaCl = 0;

if (VolFracPrecipitation_KCl < 1e-20)
VolFracPrecipitation_KCl = 0;

Scalar mNa = moleFracToMolality(volVars.moleFraction(wPhaseIdx,NaIdx), xlSalinity)
* activityCoefficient(ionicStrength(volVars),volVars.temperature(), NaIdx);
//[mol_sodium/kg_H2O]

if (mNa < 1e-20)
mNa = 0;

Scalar mCl = moleFracToMolality(volVars.moleFraction(wPhaseIdx,ClIdx), xlSalinity)
* activityCoefficient(ionicStrength(volVars),volVars.temperature(), ClIdx);
//[mol_sodium/kg_H2O]

if (mCl < 1e-20)
mCl = 0;

Scalar mK = moleFracToMolality(volVars.moleFraction(wPhaseIdx,KIdx), xlSalinity)
* activityCoefficient(ionicStrength(volVars),volVars.temperature(), KIdx);
//[mol_sodium/kg_H2O]

if (mK< 1e-20)
mK = 0;
/* function call for the solubility products of NaCl and Kcl.*/
Scalar Ksp_NaCl = solubilityProductNaCl();
Scalar Ksp_KCl = solubilityProductKCl();

/* Find the SaturationIndex(Omega) of the compounds.*/
Scalar Omega_NaCl = (mNa * mCl)/  Ksp_NaCl;
Scalar Omega_KCl = (mK * mCl) /  Ksp_KCl;

/*define the interfacial area of contact*/
Scalar Asw_NaCl = Asw0_ * pow(1-((VolFracPrecipitation_NaCl)/initialPorosity),(2.0/3.0));
Asw_NaCl = pow(Sw,nSw_NaCl); /*a functional relation of the rprecip wrt to Sw.*/

Scalar Asw_KCl = Asw0_ * pow(1-((VolFracPrecipitation_KCl)/initialPorosity),(2.0/3.0));
Asw_KCl = pow(Sw,nSw_KCl); /*a functional relation of the rprecip wrt to Sw.*/

Scalar rprec_NaCl = 0;

    if ( Omega_NaCl > =1)
    {
    rprec_NaCl = kprec_NaCl * Asw_NaCl * pow(Omega_NaCl - 1 , nprec_NaCl);	//[mol/dm³s]
    rprec_NaCl *= 1000; // rprec [mol/m³s]
    }
Scalar rprec_KCl = 0;
    elseif (Omega_KCl>= 1)
    {
    rprec_KCl = kprec_KCl * Asw_KCl * pow(Omega_KCl - 1 , nprec_KCl);	//[mol/dm³s]
    rprec_KCl *= 1000; // rprec [mol/m³s]
    }
q[wCompIdx]  = 0.0;
q[nCompIdx]  = 0.0;
q[NaIdx]= (- rprec_NaCl);
q[ClIdx]= -(rprec_NaCl+ rprec_KCl);
q[KIdx]= -(rprec_KCl);
q[NaClPhaseIdx] = (rprec_NaCl);
q[KClPhaseIdx] = (rprec_KCl);
} // for the fn. reactionSource()

private:

//Scalar ionicStrengthnacl_;

SolVector fdf_; /*Solution vector for the solvers every equation f solved by the newton solver for an unknown x */
// has to store f(x) in fdf_[0] and df/dx in fdf[1]

Vector molality_;
 Vector charge_;

static constexpr Scalar ac_ = 2000; /* [1/dm] from Anozie Ebigbo 2011 // TODO Check for value, fittet parameter!!!!!!!!!!!!*/
static constexpr Scalar kdiss1_ = 8.9e-3; // [kgH2O/dm²s] from Anozie (17)
static constexpr Scalar kdiss2_ = 6.5e-9; // [mol/dm²s] from Anozie (17)
// static constexpr Scalar kprec_ = 1.5e-12; // [mol/dm²s] from Anozie (29)change to see the mass fraction effect..
//static constexpr Scalar kprec_ = 1.9e-10;//increased the value to see the precip. according to reality..
//static constexpr Scalar kprec_ = 2.5e-10;//previous results good but still doing for better results..
static constexpr Scalar kprec_NaCl = 9e-3;/*9e-4;  //9e-5 //5e-7;	// 2.1e-9 //further increased as the mass fractions of the components a bit higher.*/
static constexpr Scalar kprec_KCl = 9e-3;
//static constexpr Scalar ndiss_ = 1.0; // TODO ask Anozie from where
//static constexpr Scalar nprec_ = 3.27; // [-] from Anozie (29)
static constexpr Scalar nprec_NaCl =  1.2;
/*1.25; //1.3   //3.25;// 3.15. //just for checking from the previous value...*/
static constexpr Scalar nprec_KCl =  1.2;
static constexpr Scalar Asw0_ = 500; // [1/dm]estimated from Anozie A/V

public:
Scalar kprec() const
{ return kprec_;}
Scalar nprec() const
{ return nprec_;}
Scalar Asw0() const
{ return Asw0_;}

static constexpr Scalar nSw_NaCl= 1;
static constexpr Scalar nSw_KCl= 1;
}; //for the class NaClK.
} //for namespace Dumux.


#endif // NACLKCLCHEM_HH
