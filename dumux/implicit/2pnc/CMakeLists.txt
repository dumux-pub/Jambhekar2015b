
install(FILES
        2pncfluxvariables.hh
        2pncindices.hh
        2pnclocalresidual.hh
        2pncmodel.hh
        2pncnewtoncontroller.hh
        2pncproperties.hh
        2pncpropertydefaults.hh
        2pncvolumevariables.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/2pnc)
