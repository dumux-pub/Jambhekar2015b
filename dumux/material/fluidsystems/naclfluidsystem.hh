#ifndef NaClFLUID_SYSTEM_HH
#define NaClFLUID_SYSTEM_HH

#include <cassert>
#include <dumux/material/idealgas.hh>

#include <dumux/material/fluidsystems/basefluidsystem.hh>
#include <dumux/material/components/brine_varSalinity.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/binarycoefficients/brine_air.hh>
#include <dumux/material/components/na.hh>
#include <dumux/material/components/cl.hh>
#include <dumux/material/components/NaCl.hh>
#include <dumux/material/binarycoefficients/h2o_air.hh>
#include <dumux/material/binarycoefficients/h2o_o2.hh>
#include <dumux/material/components/tabulatedcomponent.hh>

#include <dumux/material/fluidsystems/nullparametercache.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/exceptions.hh>

# include<assert.h>

#ifdef DUMUX_PROPERTIES_HH
#include <dumux/common/propertysystem.hh>
#include <dumux/common/basicproperties.hh>
#endif

namespace Dumux
{
namespace FluidSystems
{


template <class Scalar,
 class H2Otype = Dumux::TabulatedComponent<Scalar, Dumux::H2O<Scalar>>,
 bool useComplexRelations=true>
class NaClAir  	 	// dont confuse with  name of NaCl, still they are diff. Components.
: public BaseFluidSystem<Scalar, NaClAir<Scalar, H2Otype, useComplexRelations>>
{
 typedef NaClAir<Scalar, H2Otype, useComplexRelations> ThisType;
 typedef BaseFluidSystem <Scalar, ThisType> Base;
 typedef Dumux::IdealGas<Scalar> IdealGas;

public:

 typedef H2Otype H2O;
 typedef Dumux::BinaryCoeff::H2O_Air H2O_Air;
 typedef Dumux::Air<Scalar> Air;
 typedef Dumux::Na<Scalar> Na;
 typedef Dumux::Cl<Scalar> Cl;
 typedef Dumux::NaCl<Scalar> NaCl;
 typedef Dumux::BinaryCoeff::Brine_Air<Scalar, Air> Brine_Air;
 typedef Dumux::BrineVarSalinity<Scalar, H2Otype> Brine;

 typedef Dumux::NullParameterCache ParameterCache;

 /****************************************
 * Fluid phase related static parameters
 ****************************************/
 static const int numSecComponents = 0; //needed to set property not used for 2pncMin model
 static const int numPhases = 2; 					 // liquid and gas phases
 static const int numSPhases = 1; 					// precipitated solid phases
 static const int lPhaseIdx = 0;  					// index of the liquid phase
 static const int gPhaseIdx = 1; 					// index of the gas phase
 static const int sPhaseIdx = 2;  					// index of the precipitated salt
 static const int wPhaseIdx = lPhaseIdx; 				// index of the wetting phase
 static const int nPhaseIdx = gPhaseIdx; 				// index of the non-wetting phase

// static const int wPhaseOnly = 0;  					// only liquid phase(Water)
// static const int nPhaseOnly = 1; 					// only gas phase
// static const int bothPhases = 2;

 // export component indices to indicate the main component of the
 // corresponding phase at atmospheric pressure 1 bar and room
 // temperature 20°C: relation with liquid-water and gas-air.
 static const int wCompIdx = wPhaseIdx;
 static const int nCompIdx = nPhaseIdx;

 /*!
 * \brief Return the human readable name of a fluid phase
 *
 * \param phaseIdx The index of the fluid phase to consider
 */

 static const char *phaseName(int phaseIdx)
 {
 switch (phaseIdx) {
 case wPhaseIdx:
 return "w";
 case nPhaseIdx:
 return "n";
case sPhaseIdx:
return "s";

 };
 DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
 }


 /*!
 * \brief Return whether a phase is liquid
 *
 * \param phaseIdx The index of the fluid phase to consider
 */

 static bool isLiquid(int phaseIdx)
 {
 assert(0 <= phaseIdx && phaseIdx < numPhases);

 return phaseIdx != nPhaseIdx;
 }

 /*!
 * \brief Returns true if and only: if a fluid phase is assumed to
 * be an ideal mixture.
 *
 * We define an ideal mixture as a fluid phase where the fugacity
 * coefficients of all components times the pressure of the phase
 * are indepent on the fluid composition. This assumtion is true
 * if Henry's law and Rault's law apply. If you are unsure what
 * this function should return, it is safe to return false. The
 * only damage done will be (slightly) increased computation times
 * in some cases.
 *
 * \param phaseIdx The index of the fluid phase to consider
 */
 static bool isIdealMixture(int phaseIdx)
 {
 assert(0 <= phaseIdx && phaseIdx < numPhases);
 // we assume Henry's and Rault's laws for the water phase and
 // and no interaction between gas molecules of different
 // components, so all phases are ideal mixtures!
 // indicates the analogous thermodynamics to an ideal gas
 return true;
 }

 /*!
 * \brief Returns true if and only if : a fluid phase is assumed to
 * be compressible.
 *
 * Compressible means that the partial derivative of the density
 * to the fluid pressure is always larger than zero.
 *
 * \param phaseIdx The index of the fluid phase to consider
 */
 static bool isCompressible(int phaseIdx)
 {
 assert(0 <= phaseIdx && phaseIdx < numPhases);
 // ideal gases are always compressible. Gas in the system is assumed to be an Ideal Gas.
 if (phaseIdx == nPhaseIdx)
 return true;
 // the water component decides for the liquid phase...based over the P and temp.
 return H2O::liquidIsCompressible();
 }

 /*!
 * \brief Returns true if and only if a fluid phase is assumed to
 * be an ideal gas.
 *
 * \param phaseIdx The index of the fluid phase to consider
 */
 static bool isIdealGas(int phaseIdx)
 {
 assert(0 <= phaseIdx && phaseIdx < numPhases);

 // let the fluids decide
 if (phaseIdx == nPhaseIdx)
 return H2O::gasIsIdeal() && Air::gasIsIdeal();
 return false; 					// not a gas
 }

 /****************************************
 * Component related static parameters
 ****************************************/
 static const int numComponents = 4; 			 // H2O, Air, Na, Cl
 static const int numMajorComponents = 2; 			 // Always H2O and Air

 static const int H2OIdx = 0;
 static const int AirIdx = 1;
 static const int NaIdx = 2;
 static const int ClIdx = 3;

   /*!
 * \brief Return the human readable name of a component
 *
 * \param compIdx The index of the component to consider
 */
 static const char *componentName(int compIdx)
 {
 switch (compIdx)
 	{
 case H2OIdx:
 return H2O::name();
 case AirIdx:
 return Air::name();
 case NaIdx:
 return "Na";
 case ClIdx:
 return "Cl";
 break;
 default:DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx); break;
 	};
 }

/*!
 * \brief Return the molar mass of a component in [kg/mol].
 *
 * \param compIdx The index of the component to consider
 */
 static Scalar molarMass(int compIdx)
 {
 switch (compIdx) {
 case H2OIdx:
 return H2O::molarMass();
 case AirIdx:
 return Air::molarMass();
 case ClIdx:
 return 35.453e-3;
 case NaIdx:
 return 22.9898e-3;
 };
 DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);
 }

 static Scalar charge(int compIdx)
 {
 	Scalar z = 0;
     switch (compIdx) {
     case NaIdx: z = Na::charge();break;
     case ClIdx: z = Cl::charge();break;
     default:DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);break;
     }
     return z;
 }
 /*!
 * \brief Return the Return the constant ai Parkhurst (1990) for the modified Debye-Hückel equation
 */

static Scalar ai(int compIdx)
{
	   Scalar ai;	   	   	   	   	   	   	   //ai meaningless for uncharged species!
    switch (compIdx) {
    case NaIdx: ai = 4.25e-8;break;
    case ClIdx: ai = 3e-8;break;
    default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);break;
    }
   return ai;
}

/*!
* \brief Return the Return the constant bi Parkhurst (1990) for the modified Debye-Hückel equation
*/

static Scalar bi(int compIdx)
{
	   Scalar bi;					//bi = 0.1 for neutral species and gases!
    switch (compIdx) {
    case NaIdx: bi =  0.06;break;
    case ClIdx: bi =  0.01;break;
    default: DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << compIdx);break;
    }
   return bi;
}

 static Scalar saltDensity(int phaseIdx)	//Vishal's Remark : We need this density for the conservation of precipitated NaCl
    {
      if (phaseIdx == sPhaseIdx)
      return /*2165.0*/NaCl::Density();
    }
  static Scalar vaporPressure(Scalar T, Scalar x) //Density of solid salt phase (kg/m3)
    {
    	return vaporPressure_(T,x);
    }
    static Scalar saltSpecificHeatCapacity(int phaseIdx) /*units [J/°K . kg] From WiKi, the value is 36.79[J/°K. mol] -> so divide by the mass of salt(in kgs)*/
	{
		return 36.79/0.05844;
	}
    static Scalar saltMolarDensity(int sPhaseIdx)
     {
        return saltDensity(sPhaseIdx)/0.05844;
     }

 /****************************************
 * thermodynamic relations
 ****************************************/
 /*!
 * \brief Initialize the fluid system's static parameters generically
 *
 * If a tabulated H2O component is used, we do our best to create
 * tables that always work.
 */
 static void init ()
 {
 init(/*startTemp=*/ 273.15, /*endTemp =*/800, /*tempSteps=*/200,
 /*startPressure=*/10, /*endPressure=*/20e6, /*pressureSteps=*/200);
 }

 /*!
 * \brief Initialize the fluid system's static parameters using
 * problem specific temperature and pressure ranges
 *
 * \param tempMin The minimum temperature used for tabulation of water [K]
 * \param tempMax The maximum temperature used for tabulation of water [K]
 * \param nTemp The number of ticks on the temperature axis of the  table of water
 * \param pressMin The minimum pressure used for tabulation of water [Pa]
 * \param pressMax The maximum pressure used for tabulation of water [Pa]
 * \param nPress The number of ticks on the pressure axis of the  table of water
 */

 static void init(Scalar startTemp, Scalar endTemp, int tempSteps,
 Scalar startPressure, Scalar endPressure, int pressureSteps)
 {
 if (useComplexRelations)
 std::cout << "Using complex H2O-Air fluid system\n";
 else
 std::cout << "Using fast H2O-Air fluid system\n";

 if (H2O::isTabulated) {
 std::cout << "Initializing tables for the H2O fluid properties ("
 << tempSteps* pressureSteps
 << " entries).\n";

 H2O::init(startTemp, endTemp, tempSteps,
 startPressure, endPressure, pressureSteps);
 	}
 }

 /*!
 * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure brine.
 *
 * \param temperature temperature of component in \f$\mathrm{[K]}\f$
 * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
 *
 * Equation given in: - Batzle & Wang (1992)
 * - cited by: Bachu & Adams (2002)
 * "Equations of State for basin geofluids"
 */

 using Base::density;
 template <class FluidState>
 static Scalar density(const FluidState &fluidState,
 /*const ParameterCache &paramCache,*/
 int phaseIdx)
 {
 assert(0 <= phaseIdx && phaseIdx < numPhases);

 Scalar temperature = fluidState.temperature(phaseIdx);
 Scalar pressure = fluidState.pressure(phaseIdx);

 switch (phaseIdx)
  {
 case lPhaseIdx:
 return liquidDensity_(temperature,
			pressure,
 			fluidState.moleFraction(lPhaseIdx, AirIdx),
 			fluidState.moleFraction(lPhaseIdx, H2OIdx),
 			fluidState.massFraction(lPhaseIdx, NaIdx)
 			+ fluidState.massFraction(lPhaseIdx, ClIdx));
 case gPhaseIdx:
	return gasDensity_(temperature,
			   pressure,
			   fluidState.moleFraction(gPhaseIdx, H2OIdx));
 default:
 DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
 break;
  }
 }

 /*!
 * \brief The dynamic viscosity \f$\mathrm{[Pa*s]}\f$ of pure brine.
 *
 * \param temperature temperature of component in \f$\mathrm{[K]}\f$
 * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
 *
 * Equation given in: - Batzle & Wang (1992)
 * - cited by: Bachu & Adams (2002)
 * "Equations of State for basin geofluids"
 */
 using Base::viscosity;
 template <class FluidState>
 static Scalar viscosity(const FluidState &fluidState,
 			const ParameterCache & paramCache,
 			int phaseIdx)
 {// using Base::binaryDiffusionCoefficient;

 assert(0 <= phaseIdx && phaseIdx < numPhases);

 Scalar temperature = fluidState.temperature(phaseIdx);
 Scalar pressure = fluidState.pressure(phaseIdx);

 if (phaseIdx == lPhaseIdx)
 {
 // assume pure brine for the liquid phase. TODO: viscosity of mixture
 Scalar XSal = fluidState.massFraction(lPhaseIdx, NaIdx)
 		+fluidState.massFraction(lPhaseIdx, ClIdx);
 Scalar result = Brine::liquidViscosity(temperature, pressure, XSal); // Viscosity of pure water is taken into account
 Valgrind::CheckDefined(result);
 return (result);
 }
 else if (phaseIdx == gPhaseIdx)
 {
 Scalar result = Air::gasViscosity(temperature, pressure);
 Valgrind::CheckDefined(result);
 return (result);
 }
 else
 DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
}

/*!
 * \brief Returns the fugacity coefficient [Pa] of a component in a
 * phase.
 *
 * The fugacity coefficient \f$\phi^\kappa_\alpha\f$ of a
 * component \f$\kappa\f$ for a fluid phase \f$\alpha\f$ defines
 * the fugacity \f$f^\kappa_\alpha\f$ by the equation
 *
 * \f[
 f^\kappa_\alpha := \phi^\kappa_\alpha x^\kappa_\alpha p_\alpha\;.
 \f]
 *
 * The fugacity itself is just an other way to express the
 * chemical potential \f$\zeta^\kappa_\alpha\f$ of the component:
 *
 * \f[
 f^\kappa_\alpha := \exp\left\{\frac{\zeta^\kappa_\alpha}{k_B T_\alpha} \right\}
 \f]
 * where \f$k_B\f$ is Boltzmann's constant.
 */

 using Base::fugacityCoefficient;
 template <class FluidState>
 static Scalar fugacityCoefficient(const FluidState &fluidState,
                                   int phaseIdx,
                                   int compIdx)
 {
     assert(0 <= phaseIdx && phaseIdx < numPhases);
     assert(0 <= compIdx && compIdx < numComponents);

     Scalar T = fluidState.temperature(phaseIdx);
     Scalar p = fluidState.pressure(phaseIdx);
     /* Scalar s = fluidState.saturation(phaseIdx); commented for now not being used
     tried this earlier to check y the fingers effect was happening.*/
     assert(T > 0);
     assert(p > 0);
     

     if (phaseIdx == gPhaseIdx)
     	return 1.0;

     else if (phaseIdx == lPhaseIdx)
     {
     	if (compIdx == H2OIdx)
				return H2O::vaporPressure(T)/p;
			else if (compIdx == AirIdx)
				return Dumux::BinaryCoeff::H2O_Air::henry(T)/p;
			else
				return 1/((p));
     }
     else
     	DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
 }
/*!
 * \brief Calculate the molecular diffusion coefficient for a
 * component in a fluid phase [mol^2 * s / (kg*m^3)]
 *
 * Molecular diffusion of a compoent \f$\kappa\f$ is caused by a
 * gradient of the chemical potential and follows the law
 *
 * \f[ J = - D \grad mu_\kappa \f]
 *
 * where \f$\mu_\kappa\f$ is the component's chemical potential,
 * \f$D\f$ is the diffusion coefficient and \f$J\f$ is the
 * diffusive flux. \f$mu_\kappa\f$ is connected to the component's
 * fugacity \f$f_\kappa\f$ by the relation
 *
 * \f[ \mu_\kappa = R T_\alpha \mathrm{ln} \frac{f_\kappa}{p_\alpha} \f]
 *
 * where \f$p_\alpha\f$ and \f$T_\alpha\f$ are the fluid phase'
 * pressure and temperature.
 *
 * \param fluidState An abitrary fluid state
 * \param phaseIdx The index of the fluid phase to consider
 * \param compIdx The index of the component to consider
 */

 using Base::diffusionCoefficient;
 template <class FluidState>
 static Scalar diffusionCoefficient(const FluidState &fluidState,
                                    int phaseIdx,
                                    int compIdx)
 {
     // TODO!
     DUNE_THROW(Dune::NotImplemented, "Diffusion coefficients");
 };


     /*!
     * \brief Given the phase compositions, return the binary
     *        diffusion coefficent of two components in a phase.
     */

using Base::binaryDiffusionCoefficient;
template <class FluidState>
static Scalar binaryDiffusionCoefficient(const FluidState &fluidState,
                                         int phaseIdx,
                                         int compIIdx,
                                         int compJIdx)
{
 assert(0 <= phaseIdx && phaseIdx < numPhases);
 assert(0 <= compIIdx && compIIdx < numComponents);
 assert(0 <= compJIdx && compJIdx < numComponents);

 Scalar temperature = fluidState.temperature(phaseIdx);
 Scalar pressure = fluidState.pressure(phaseIdx);

 if (phaseIdx == lPhaseIdx)
    {
	 assert(compIIdx == H2OIdx);
	 assert(compJIdx == AirIdx || compJIdx == NaIdx || compJIdx == ClIdx );
		Scalar result = 0.0;
		  if(compJIdx == AirIdx)
		  result = H2O_Air::liquidDiffCoeff(temperature, pressure);
			else if (compJIdx == NaIdx)
			result = 1.334e-9; 		//[m²/s]
		else if (compJIdx == ClIdx)
			result = 2.032e-9;		//[m²/s] // data from „http://akasha.wsu.edu/~flury/theses_articles/mosa1.pdf“
		else
		DUNE_THROW(Dune::NotImplemented,"Binary difussion coefficient : Incorrect compIdx");
		Valgrind::CheckDefined(result);
		return result;
    }

	else {
		assert(phaseIdx == gPhaseIdx);

		if (compIIdx != AirIdx)
		std::swap(compIIdx, compJIdx);

assert(compIIdx == AirIdx);
//if (compJIdx != H2OIdx)
//{
//	std::cout<<"binaryDiffusionCoefficient, airphase: compJIdx is not H2O!, compJIdx = "<<compJIdx<<std::endl;
//}
assert(compJIdx == H2OIdx || compJIdx == NaIdx || compJIdx == ClIdx);
	  Scalar result = 0.0;
	  if(compJIdx == H2OIdx)
	result = H2O_Air::gasDiffCoeff(temperature, pressure);
		else if (compJIdx == NaIdx)
			result = 2.0e-4;  //TODO high, non-zero value to prevent unphysical solutions
		else if (compJIdx == ClIdx)
			result = 2.0e-4;  //TODO added though not practical due to the spread of the unphysical values in the gas phase.
		else
		DUNE_THROW(Dune::NotImplemented, "Binary difussion coefficient : Incorrect compIdx");
		Valgrind::CheckDefined(result);
		return result;
	}
    };

/*!
 * \brief Given the phase composition, return the specific
 * phase enthalpy [J/kg].
 */
using Base::enthalpy;
 template <class FluidState>
 static Scalar enthalpy(const FluidState &fluidState, /*const ParameterCache &paramCache,*/
 int phaseIdx)
 {
 assert(0 <= phaseIdx && phaseIdx < numPhases);

 Scalar temperature = fluidState.temperature(phaseIdx);
 Scalar pressure = fluidState.pressure(phaseIdx);

 	 if (phaseIdx == lPhaseIdx)
 	 {
      Scalar XlSal = fluidState.massFraction(lPhaseIdx, NaIdx)
    		         + fluidState.massFraction(lPhaseIdx, ClIdx);
      Scalar result = liquidEnthalpyBrine_(temperature,
                                           pressure,
                                           XlSal);
            Valgrind::CheckDefined(result);
            return result;
	}
	else
	{
		Scalar XAir = fluidState.massFraction(gPhaseIdx, AirIdx);
		Scalar XH2O = fluidState.massFraction(gPhaseIdx, H2OIdx);

		Scalar result = 0;
		result += XH2O * H2O::gasEnthalpy(temperature, pressure);
		result += XAir * Air::gasEnthalpy(temperature, pressure);
		Valgrind::CheckDefined(result);
		return result;
	}
};

/*!
* \brief Returns the specific enthalpy [J/kg] of a component in a specific phase
*/
template <class FluidState>
static Scalar componentEnthalpy(const FluidState &fluidState,
                                int phaseIdx,
                                int componentIdx)
{
    Scalar T = fluidState.temperature(nPhaseIdx);
    Scalar p = fluidState.pressure(nPhaseIdx);
    Valgrind::CheckDefined(T);
    Valgrind::CheckDefined(p);

    if (phaseIdx == wPhaseIdx)
    {
        DUNE_THROW(Dune::NotImplemented, "The component enthalpies in the liquid phase are not implemented.");
    }
    else if (phaseIdx == nPhaseIdx)
    {
        if (componentIdx ==  H2OIdx)
        {
            return H2O::gasEnthalpy(T, p);
        }
        else if (componentIdx == AirIdx)
        {
            return Air::gasEnthalpy(T, p);
        }
        DUNE_THROW(Dune::InvalidStateException, "Invalid component index " << componentIdx);
    }
    DUNE_THROW(Dune::InvalidStateException, "Invalid phase index " << phaseIdx);
}

 /*!
 * \brief Thermal conductivity of a fluid phase [W/(m^2 K/m)].
 *
 * \param fluidState An abitrary fluid state
 * \param phaseIdx The index of the fluid phase to consider
 */

 using Base::thermalConductivity;
 template <class FluidState>
 static Scalar thermalConductivity(const FluidState &fluidState,
                                   int phaseIdx)
 {
 // TODO way too simple!
 if (phaseIdx == lPhaseIdx)
 return 0.60768; 			/* conductivity of water[W / (m K ) source : http://www.nist.gov/data/PDFfiles/jpcrd493.pdf]*/

 else 	// gas phase
 return 0.0255535; 			/* conductivity of air [W / (m K ) source: http://hyperphysics.phy-astr.gsu.edu/hbase/tables/thrcn.html ]*/
 }

/*!
 * \brief Specific isobaric heat capacity of a fluid phase.
 * [J/kg]		For those cases with constant Pressures.
 *
 * \param fluidState An abitrary fluid state
 * \param phaseIdx The index of the fluid phase to consider
 */

 using Base::heatCapacity;
 template <class FluidState>
 static Scalar heatCapacity(const FluidState &fluidState,
		 	 	 	 	 	 const ParameterCache &paramCache,
		 	 	 	 	 	 int phaseIdx)
 {
 // TODO!
 DUNE_THROW(Dune::NotImplemented, "Heat capacities");
 }

private:
 static Scalar gasDensity_(Scalar T,
 			       	       Scalar pg,
                           Scalar xgH2O)
 {
 Scalar pH2O = xgH2O*pg; 		//Dalton' Law
 Scalar pAir = pg - pH2O;
 Scalar gasDensityAir = Air::gasDensity(T, pAir);
 Scalar gasDensityH2O = H2O::gasDensity(T, pH2O);
 Scalar gasDensity = gasDensityAir + gasDensityH2O;
 return gasDensity;
 }

 /*!
 * \brief The density of pure brine at a given pressure and temperature \f$\mathrm{[kg/m^3]}\f$.
 *
 * \param temperature temperature of component in \f$\mathrm{[K]}\f$
 * \param pressure pressure of component in \f$\mathrm{[Pa]}\f$
 *
 * Equations given in: - Batzle & Wang (1992)
 * - cited by: Adams & Bachu in Geofluids (2002) 2, 257-271
 */

 static Scalar liquidDensity_(Scalar T,
							  Scalar pl,
							  Scalar xlAir,
							  Scalar xlH2O,
							  Scalar XlSal)
 {
 Valgrind::CheckDefined(T);
 Valgrind::CheckDefined(pl);
 Valgrind::CheckDefined(XlSal);
 Valgrind::CheckDefined(xlAir);

 if(T < 273.15 || T > 623.15) {
 DUNE_THROW(NumericalProblem,
 "Liquid density for Brine and Air is only "
 "defined between 273.15K and 623.15K (is " << T << ")");
 }
 if(pl >= 1.0e8) {
 DUNE_THROW(NumericalProblem,
 "Liquid density for Brine and Air is only "
 "defined below 100MPa (is " << pl << ")");
 }

 Scalar rho_brine = Brine::liquidDensity(T, pl, XlSal);
 Scalar rho_pure = H2O::liquidDensity(T, pl);
 Scalar rho_lAir = liquidDensityWaterAir_(T, pl, xlH2O, xlAir);
 Scalar contribAir = rho_lAir - rho_pure;
 return rho_brine+ contribAir;
 }

 static Scalar liquidDensityWaterAir_(Scalar temperature,
									  Scalar pl,
									  Scalar xlH2O,
									  Scalar xlAir)
 {
 const Scalar M_Air = Air::molarMass();
 const Scalar M_H2O = H2O::molarMass();

 const Scalar tempC = temperature - 273.15;		 /* tempC : temperature in °C */
 const Scalar rho_pure = H2O::liquidDensity(temperature, pl);
 xlH2O = 1.0 -  xlAir;				 // xlH2O is available, but in case of a pure gas phase
 // the value of M_T for the virtual liquid phase can become very large
 const Scalar M_T = M_H2O * xlH2O + M_Air * xlAir;		// total mass of the liquid.
 const Scalar V_phi = (37.51 + tempC*(-9.585e-2 + tempC*(8.74e-4 - tempC*5.044e-7))) / 1.0e6;
 return 1/ (xlAir * V_phi/M_T + M_H2O * xlH2O / (rho_pure * M_T));
 }

 static Scalar liquidEnthalpyBrine_(Scalar T,
									Scalar p,
									Scalar Salinity)
 {
     /* XlAir : mass fraction of Air in brine */
     /* same function as enthalpy_brine, only extended by Air content */
     /*Numerical coefficents from PALLISER*/

     static const Scalar f[] =
     {2.63500E-1, 7.48368E-6, 1.44611E-6, -3.80860E-10};

     /*Numerical coefficents from MICHAELIDES for the enthalpy of brine*/
     static const Scalar a[4][3] =
     {{ 9633.6, -4080.0, +286.49 },
         { +166.58, +68.577, -4.6856 },
         { -0.90963, -0.36524, +0.249667E-1 },
         { +0.17965E-2, +0.71924E-3, -0.4900E-4 }};

     Scalar theta, h_NaCl;
     Scalar m, h_ls, d_h, hw;
     Scalar S_lSAT, delta_h;
     int i, j;
     Salinity = std::abs(Salinity); // Added by Vishal

     theta = T - 273.15;

     S_lSAT = f[0] + f[1]*theta + f[2]*theta*theta + f[3]*theta*theta*theta;
     /*Regularization*/
     if (Salinity > S_lSAT) {
         Salinity = S_lSAT;
     }

     hw = H2O::liquidEnthalpy(T, p) /1E3; /* kJ/kg */

     /*DAUBERT and DANNER*/
     /*U=*/h_NaCl = (3.6710E4*T + 0.5*(6.2770E1)*T*T - ((6.6670E-2)/3)*T*T*T // enthalpy of Halite (Confirm with Dr. Melani)
                     +((2.8000E-5)/4)*(T*T*T*T))/(58.44E3)- 2.045698e+02; /* kJ/kg */

     m = (1E3/58.44)*(Salinity/(1-Salinity));
     i = 0;
     j = 0;
     d_h = 0;

     for (i = 0; i<=3; i++) {
         for (j=0; j<=2; j++) {
             d_h = d_h + a[i][j] * pow(theta, i) * pow(m, j);
         }
     }
     /* heat of dissolution for halite according to Michaelides 1971 */
     delta_h = (4.184/(1E3 + (58.44 * m)))*d_h;

     /* Enthalpy of brine without Air */
     h_ls = (1-Salinity)*hw + Salinity*h_NaCl + Salinity*delta_h; /* kJ/kg */
     return (h_ls);
 };

 static Scalar vaporPressure_(Scalar T, Scalar x)
 {
	 Scalar p_o = H2O::vaporPressure(T); /* this is from the main dumux stable portion and this refers to pure water.*/
	 Scalar p_s = p_o; /* this is used for the modification based on the saline water.*/
	// Scalar vw = 18e-6; /* this is the volume occupied by a single mole of water. [m³/mol]*/
	// Scalar R = 8.314; /* Universal Gas const, J/K.mol*/
	// Scalar pi = (R*T*std::log(1-x)/vw); /* units = J/m³.. Strange units ask?*/
	//Scalar ps = p_o*std::exp((pi*vw)/(R*T)); /* this is for the osmotic pressure case.*/
	 return (p_s);
/* the above were commented due to the fact that the constraint solver has the
 * data already implemented within and including the above would cause to modify the calculation.*/
 }
public:
};

} // end namepace
} // end namepace

#endif



